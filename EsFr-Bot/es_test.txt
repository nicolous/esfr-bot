block load
{
    es_set unevariable 0
    es_msg C'est le plus beau script que je n'ai jamais écris !
}

event item_pickup
{
    // creation d'une variable
    es_set nom_arme 0
    // ajouter "weapon_" devant
    es_format nom_arme "weapon_%1" event_var(item)
    // supprimer l'arme
    es_fire event_var(userid) server_var(nom_arme) kill
}

block unload
{
    es_msg Quel dommage, ce script était pourtant si bien...
}