#    Copyright 2009-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESSParser.
#
#    ESSParser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESSParser is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESSParser.  If not, see <http://www.gnu.org/licenses/>.

######################################
# KeyValues lexical analyser (lexer)
######################################

import sys

# Note: using "from ply import *" confuses py2exe
import ply.lex as lex
from ply.lex import TOKEN
import ply.yacc as yacc

class KvLexer(object):
    """KeyValues lexer"""

    # Tokens which will be sent to the [yacc] parser
    tokens = ("ID","LBRACKET","RBRACKET","COMMENT")

    def __init__(self,output,debug):
        """Build the lexer
        output: baseoutput.BaseOutput instance
        debug: Debug mode enabled?
        """
        # File currently parsed
        self.__fileparsed = ""
        
        # Output stream
        self.__output = output
        
        # Ply lexer instance
        self.__lexer = lex.lex(module=self, debug=debug)
        
        # Right and left brackets count
        self.__rBracketsCount = 0
        self.__lBracketsCount = 0
        
    @staticmethod
    def getYaccTokens():
        """Returns the token list indented to yacc"""
        tokenlist = list(KvLexer.tokens)
        tokenlist.remove("COMMENT")
        return tokenlist        

    def setFileParsed(self,filepath):
        """filepath will only be used to send error messages"""
        self.__fileparsed = filepath
        
    def getBracketsCount(self):
        """Retrieve the encountered brackets count
        Return a (right brackets count, left brackets count) tuple
        """
        return (self.__rBracketsCount,self.__lBracketsCount)
        
        
    def getLineno(self):
        """Line where the parser is"""
        return self.__lexer.lineno
        
               
    #################################
    ######### Lex methods: ##########
    #################################
    
    def input(self, text):
        self.__rBracketsCount = 0
        self.__lBracketsCount = 0        
        self.__lexer.lineno = 1
        
        self.__lexer.input(text)

    def token(self):
        return self.__lexer.token()


    #################################
    ########## Lex rules: ###########
    #################################
    # Note: Use functions instead of 't_SOMETOKEN = pattern', the check order will be kept by the lex    
         
    # Ignore rule for spaces & tab (using ply t_STATE_ignore_something feature)         
    t_ANY_ignore = " \t"

    # Enf of line rule
    @TOKEN(r"(\r?\n)+")
    def t_EOL(self,t):
        t.lexer.lineno += t.value.count("\n")
        return None
    
    # Comment rule
    @TOKEN(r"//[^\r\n]*")
    def t_COMMENT(self,t):
        return t
    
    @TOKEN("{")
    def t_LBRACKET(self,t):
        self.__lBracketsCount += 1
        return t
                    
    @TOKEN("}")
    def t_RBRACKET(self,t):
        self.__rBracketsCount += 1
        return t
    
    # What is an identifier in KeyValues?
    #   "one or more words with or without escape chars"
    # or
    #   one_word_without_escape_chars    
    @TOKEN("(\"[^\"]*\")|([^\s{}\"]+)")
    def t_ID(self,t):
        return t

    # Error rule
    def t_error(self,t):
        self.__output.error(
            self.__fileparsed,
            t.lexer.lineno,
            _("Unexpected character '%(char)s'") % {"char" : t.value[0]})
        t.lexer.skip(1)




