#    Copyright 2009-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESSParser.
#
#    ESSParser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESSParser is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESSParser.  If not, see <http://www.gnu.org/licenses/>.

from baseoutput import *

import sys
import os

def __getStdoutEncoding():
    """Guess the encoding to use for std output."""
    encoding = sys.stdout.encoding
    if encoding is None: # if output is piped...
        encoding = "utf-8"
    return encoding
        
ENCODING = __getStdoutEncoding()

class ConsoleOutput(BaseOutput):
    """Console output
    Errors & warnings are sent on the error channel
    """

    def __init__(self):
        BaseOutput.__init__(self)
        self.__warningCount = 0
        self.__errorCount = 0        
        self.__noticeCount = 0             


    def warningCount(self):
        return self.__warningCount

    def errorCount(self):
        return self.__errorCount

    def noticeCount(self):
        return self.__noticeCount

    
    def clear(self):
        self.__warningCount = 0
        self.__errorCount = 0        
        self.__noticeCount = 0
        

    def warning(self,scriptpath,lineno,message):
        self.__warningCount += 1
        message = _("Warning in file %(filepath)s at line %(linenum)s: %(message)s\n") \
            % {"filepath" : os.path.basename(scriptpath), "linenum" : lineno, "message" : message}
        sys.stderr.write(message.encode(ENCODING,"replace"))

    def error(self,scriptpath,lineno,message):
        self.__errorCount += 1
        message = _("Error in file %(filepath)s at line %(linenum)s: %(message)s\n") \
            % {"filepath" : os.path.basename(scriptpath), "linenum" : lineno, "message" : message}
        sys.stderr.write(message.encode(ENCODING,"replace"))

    def notice(self,scriptpath,lineno,message):
        self.__noticeCount += 1
        message = _("In file %(filepath)s at line %(linenum)s: %(message)s\n") \
            % (os.path.basename(scriptpath),lineno,message)
        sys.stdout.write(message.encode(ENCODING,"replace"))
            
