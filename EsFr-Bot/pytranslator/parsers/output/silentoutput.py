#    Copyright 2009-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESSParser.
#
#    ESSParser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESSParser is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESSParser.  If not, see <http://www.gnu.org/licenses/>.

from baseoutput import *

class SilentOutput(BaseOutput):
    """No real output, just count the errors"""


    def __init__(self):
        BaseOutput.__init__(self)
        self.__warningCount = 0
        self.__errorCount = 0        
        self.__noticeCount = 0     


    def warningCount(self):
        return self.__warningCount

    def errorCount(self):
        return self.__errorCount

    def noticeCount(self):
        return self.__noticeCount

    
    def clear(self):
        self.__warningCount = 0
        self.__errorCount = 0        
        self.__noticeCount = 0

    def warning(self,scriptpath,lineno,message):
        self.__warningCount += 1

    def error(self,scriptpath,lineno,message):
        self.__errorCount += 1

    def notice(self,scriptpath,lineno,message):
         self.__noticeCount += 1
