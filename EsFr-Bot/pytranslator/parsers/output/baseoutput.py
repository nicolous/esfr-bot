#    Copyright 2009-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESSParser.
#
#    ESSParser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESSParser is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESSParser.  If not, see <http://www.gnu.org/licenses/>.

class BaseOutput(object):
    """Output interface for the parser"""

    def warningCount(self):
        raise ValueError("Implement me")

    def errorCount(self):
        raise ValueError("Implement me")

    def noticeCount(self):
        raise ValueError("Implement me")

    def clear(self):
        raise ValueError("Implement me")

    def warning(self,scriptpath,lineno,message):
        raise ValueError("Implement me")

    def error(self,scriptpath,lineno,message):
        raise ValueError("Implement me")

    def notice(self,scriptpath,lineno,message):
        raise ValueError("Implement me")
