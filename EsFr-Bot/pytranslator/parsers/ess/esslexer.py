#    Copyright 2009-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESSParser.
#
#    ESSParser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESSParser is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESSParser.  If not, see <http://www.gnu.org/licenses/>.

"""ESShell lexical analyser (lexer)

 Two goals:
   - Recognize the identifiers
   - Recognize the keywords/symbols structuring the blocks/events
   - Stick as much as possible with the ES interpretation

 The rest is console commands since:
   - each line is independant (even if/ifx and else),
   - keywords can be used as argument,
   - the arguments could be resolved at runtime
 e.g.:
   if (server_var(arg1) server_var(operator) server_var(arg2)) do
   => Here the operator is not already known (imagine how corelib's "while" works...)
   In addition, the corresponding block does not necessarely follow the if...do
 Improvements are welcome. Because of that the grammar is... weirdly short, and all scripts could not be parsed

 ESS escape chars are supported
"""

import sys

# Note: using "from ply import *" confuses py2exe
import ply.lex as lex
from ply.lex import TOKEN
import ply.yacc as yacc

class EssLexer(object):
    """ESShell lexer"""

    # Language keywords
    __keywords = {
            "block" : "BLOCK",
            "event" : "EVENT"
    }

    # Tokens which will be sent to the [yacc] parser
    tokens = ("EOL","ID","SEMICOLON","LBRACKET","RBRACKET","COMMENT") + tuple(__keywords.values())
    # "VAR",

    # Chars that have to be quoted to not be considered as an independant argument
    __eventscripts_escapechars = "; {}()':"

    # Lexer's states
    states = (
        ("GLOBALSCOPE","inclusive"), # parser is out of block
        ("CONCODE","inclusive"), # parser is inside a block: all lines are console commands except { }
    )
    
    @staticmethod
    def getYaccTokens():
        """Returns the token list indented to yacc"""
        tokenlist = list(EssLexer.tokens)
        tokenlist.remove("COMMENT")
        return tokenlist    
    
    def __init__(self,output,debug):
        """Build the lexer
        output: baseoutput.BaseOutput instance
        debug: Debug mode enabled?
        """
        # Script currently parsed
        self.__fileparsed = ""
        
        # Output stream
        self.__output = output
        
        # Ply lexer instance
        self.__lexer = lex.lex(module=self,debug=debug)
        self.__lexer.begin("GLOBALSCOPE")   
        
        # Is the parser at the begin of a new line?
        self.__newline = True
        
        # Right and left brackets count
        self.__rBracketsCount = 0
        self.__lBracketsCount = 0

    def setFileParsed(self,filepath):
        """filepath will only be used to send error messages"""
        self.__fileparsed = filepath
        
    def getBracketsCount(self):
        """Retrieve the encountered brackets count
        Return a (right brackets count, left brackets count) tuple
        """
        return (self.__rBracketsCount,self.__lBracketsCount)
        
        
    def getLineno(self):
        """Line where the parser is"""
        return self.__lexer.lineno
        
               
    def __getKeywordType(self,token):
        """Is this token a keyword? If no, return None. Return the token type otherwise"""
        keywordType = None
        if self.__newline:
            keywordType = EssLexer.__keywords.get(token,None)
        return keywordType


    #################################
    ######### Lex methods: ##########
    #################################
    
    def input(self, text):
        self.__lexer.begin("GLOBALSCOPE")
        self.__rBracketsCount = 0
        self.__lBracketsCount = 0        
        self.__newline = True
        self.__lexer.lineno = 1
        
        self.__lexer.input(text)

    def token(self):
        return self.__lexer.token()


    #################################
    ########## Lex rules: ###########
    #################################
    # Note: Use functions instead of 't_SOMETOKEN = pattern', the check order will be kept by the lex
    
    # Ignore rule for spaces & tab (using ply t_STATE_ignore_something feature)         
    t_ANY_ignore = " \t"    

    # End of line rule    
    @TOKEN(r"(\r?\n)+")
    def t_ANY_EOL(self,t):
        t.lexer.lineno += t.value.count("\n")
        self.__newline = True
        return t
        
    # Comment rule
    @TOKEN(r"//[^\r\n]*")
    def t_ANY_COMMENT(self,t):
        return t
        
#    @TOKEN(r"((server)|(event))_var")
#    def t_CONCODE_VAR(self,t):
#        self.__newline = True
#        t.type = "ID"
#        return t
        
    # Escape chars rules
    @TOKEN(";")
    def t_ANY_SEMICOLON(self,t):
        self.__newline = False
        return t

    @TOKEN("{")
    def t_ANY_LBRACKET(self,t):
        if not self.__newline:
            t.type = "ID"
        else:
            self.__rBracketsCount += 1
            t.lexer.push_state("CONCODE")
        self.__newline = False
        return t
                    
    @TOKEN("}")
    def t_CONCODE_RBRACKET(self,t):
        if not self.__newline:
            t.type = "ID"
        else:
            self.__lBracketsCount += 1
            t.lexer.pop_state()
        self.__newline = False
        return t

    @TOKEN("}")
    def t_GLOBALSCOPE_RBRACKET(self,t):
        return t

    @TOKEN("\(")
    def t_ANY_LPAREN(self,t):
        t.type = "ID"
        self.__newline = False	
        return t

    @TOKEN("\)")
    def t_ANY_RPAREN(self,t):
        t.type = "ID"
        self.__newline = False
        return t
            
    @TOKEN("'")
    def t_ANY_QUOTE(self,t):
        t.type = "ID"
        self.__newline = False
        return t

    @TOKEN(":")
    def t_ANY_COLON(self,t):
        t.type = "ID"
        self.__newline = False
        return t
            
           
    # Identifiers rules (depending to the lexer state)
    
    # What is an identifier in ESShell?
    #   "one or more words with or without escape chars"
    # or
    #   one_word_without_escape_chars    
    __identifier = "(\"[^\"\r\n]*\")|([^%s\r\n\"]+)" % (__eventscripts_escapechars)
    
    #     GLOBALSCOPE:
    @TOKEN(__identifier)
    def t_GLOBALSCOPE_ID(self,t):
        tempType = self.__getKeywordType(t.value)
        
        # Any identifier corresponding to a keyword will be handled as a keyword, not as an argument
        if tempType is not None:
            t.type = tempType

        self.__newline = False
        
        return t
        
    #    CONCODE:
    @TOKEN(__identifier)
    def t_CONCODE_ID(self,t):
        return t


    # Error rule
    def t_ANY_error(self,t):
        self.__output.error(
            self.__fileparsed,
            t.lexer.lineno,
            _("Unexpected character '%(char)s'") % {"char" : t.value[0]})
        t.lexer.skip(1)
