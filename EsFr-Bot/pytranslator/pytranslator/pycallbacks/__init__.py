import sys
sys.path.append("..")

import internal_callbacks
import corelib_callbacks
import downloadable_callbacks
import usermsg_callbacks
import foreach_callbacks
import getplayercount_callbacks
#import popup_callbacks

command_callbacks = {}

for callbacks in (internal_callbacks.command_callbacks,
             corelib_callbacks.command_callbacks,
             downloadable_callbacks.command_callbacks,
             usermsg_callbacks.command_callbacks,
             foreach_callbacks.command_callbacks,
             getplayercount_callbacks.command_callbacks,
             #popup_callbacks.command_callbacks
             ):
    for command in callbacks:
        command_callbacks[command] = callbacks[command]
        
