import pytranslator
from pytranslator.pytranslator import EssTranslator
from pytranslator.pytranslator import EssSyntaxError

import sys
me = sys.modules[__name__]

def translate_usermsg(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 3:
        raise EssSyntaxError(lineno, args, "Syntax error: usermsg <subcommand> <parameters>")

    translatorObj.addImport("usermsg\n")

    subcommand = EssTranslator.expand(args[1], False, expanded)
    callback_name = "translate_usermsg_" + subcommand.strip("'")
    if hasattr(me, callback_name):
        translation += getattr(me, callback_name)(translatorObj, lineno, args, expanded, indent)
    else:
        translation += '%ses.server.cmd("%s' %(indent, args[0])
        for arg in args[1:]:
            arg = EssTranslator.expand(arg, False, expanded)
            if arg.startswith('str('):
                translation += ' "+%s+"' % arg
            else:
                translation += ' %s' % arg
        translation += '")\n'

    return translation
        

def translate_usermsg_fade(translatorObj, lineno, args, expanded, indent):
    translation = ""

    if len(args) < 10:
        raise EssSyntaxError(lineno, args, "Syntax error: usermsg fade <userid> <fade> <time fade> <time blind> <red> <green> <blue> <alpha>")

    userid = EssTranslator.expand(args[2], False, expanded)
    translation += '%susermsg.fade(%s' %(indent, userid)
    
    for arg in args[3:]:
        temp = EssTranslator.expand(arg, True, expanded)
        translation += ', %s' % temp
    translation += ')\n'

    return translation    

def translate_usermsg_shake(translatorObj, lineno, args, expanded, indent):
    translation = ""

    if len(args) < 5:
        raise EssSyntaxError(lineno, args, "Syntax error: usermsg shake <userid> <magnitude> <time>")

    userid = EssTranslator.expand(args[2], False, expanded)
    translation += '%susermsg.shake(%s' %(indent, userid)

    for arg in args[3:]:
        temp = EssTranslator.expand(arg, True, expanded)
        translation += ', %s' % temp
    translation += ')\n'

    return translation

def translate_usermsg_motd(translatorObj, lineno, args, expanded, indent):
    translation = ""

    if len(args) < 6:
        raise EssSyntaxError(lineno, args, "Syntax error: usermsg motd <userid> <0 = text, 2 = url> <title> <\"msg\">")

    userid = EssTranslator.expand(args[2], False, expanded)
    translation += '%susermsg.motd(%s' % (indent, userid)

    for arg in args[3:]:
        temp = EssTranslator.expand(arg, False, expanded)
        translation += ', %s' % temp
    translation += ')\n'

    return translation

def translate_usermsg_hudhint(translatorObj, lineno, args, expanded, indent):
    translation = ""

    if len(args) < 4:
        raise EssSyntaxError(lineno, args, "Syntax error: usermsg hudhint <userid> <\"msg\">")
    
    userid = EssTranslator.expand(args[2], False, expanded)
    message = EssTranslator.expand(args[3], False, expanded)
    
    translation += '%susermsg.hudhint(%s, %s)\n' % (indent, userid, message)

    return translation
    

def translate_usermsg_centermsg(translatorObj, lineno, args, expanded, indent):
    translation = ""

    if len(args) < 4:
        raise EssSyntaxError(lineno, args, "Syntax error: usermsg centermsg <userid> <\"msg\">")
    
    userid = EssTranslator.expand(args[2], False, expanded)
    message = EssTranslator.expand(args[3], False, expanded)
    
    translation += '%susermsg.centermsg(%s, %s)\n' % (indent, userid, message)

    return translation
    

def translate_usermsg_echo(translatorObj, lineno, args, expanded, indent):
    translation = ""

    if len(args) < 4:
        raise EssSyntaxError(lineno, args, "Syntax error: usermsg echo <userid> <\"msg\">")
    
    userid = EssTranslator.expand(args[2], False, expanded)
    message = EssTranslator.expand(args[3], False, expanded)
    
    translation += '%susermsg.echo(%s, %s)\n' % (indent, userid, message)

    return translation

command_callbacks = {
    "usermsg"       : translate_usermsg,
}
