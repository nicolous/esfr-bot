import pytranslator
from pytranslator.pytranslator import EssTranslator
from pytranslator.pytranslator import EssSyntaxError

import ess.essparser as essparser
import ess.esslexer as esslexer

import sys
me = sys.modules[__name__]

def translate_foreach(translatorObj, lineno, args, expanded, indent):
    translation = ""

    if len(args) < 2:
        raise EssSyntaxError(lineno, args, "Syntax Error: foreach <subcommand>")

    subcommand = EssTranslator.expand(args[1], False, expanded)
    callback_name = 'translate_foreach_%s' % subcommand.strip("'")

    if hasattr(me, callback_name):
        translation += getattr(me, callback_name)(translatorObj, lineno, args, expanded, indent)
    else:
        raise EssSyntaxError(lineno, args, "Syntax Error: foreach <subcommand> can be player, entity, weapon, part, token")

    return translation

def __ParseLineCommand(translatorObj, argument):

    command = EssTranslator.stripQuotes(argument)
    temp_block = "block temp_block\n{\n    %s\n}\n" % command
    
    scriptpath = translatorObj.getScript().getPath()
    output = translatorObj.getOutput()
    
    lexer = esslexer.EssLexer(output, False)
    parser = essparser.EssParser(output, False, lexer)
    
    script = parser.parse(scriptpath, temp_block, False)
    blockname = script.getBlocks()
    commandline = blockname[0].getDefinition()

    return commandline

    

def translate_foreach_player(translatorObj, lineno, args, expanded, indent):
    translation = ""

    if len(args) < 5:
        raise EssSyntaxError(lineno, args, "Syntax Error: foreach player <variable> <identifier> \"<command>\"")

    translatorObj.addImport("playerlib\n")

    identifiers = args[3].replace('#', ',#')[1:]
    varname = EssTranslator.expand(args[2], False, expanded)


    translation += "%sfor x in playerlib.getUseridList('%s'):\n" % (indent, identifiers)
    translation += "%s    emlib.getConVar(%s, True).set(x)\n" %(indent, varname)
            
    new_command = __ParseLineCommand(translatorObj, args[4])

    for code in new_command:
        translation += translatorObj.translateParsedArgs(code.getLineno(), EssTranslator.parseArguments(code.getLine()), True, indent+'    ')

    return translation

def translate_foreach_entity(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 6:
        raise EssSyntaxError(lineno, args, "foreach entity <index-variable> <class-variable> <identifier> <command> ")

    indexname = EssTranslator.expand(args[2], False, expanded)
    classname = EssTranslator.expand(args[3], False, expanded)
    identifier = EssTranslator.expand(args[4], False, expanded)

    translation += "%sfor x in es.getEntityIndexes(%s):\n" %(indent, identifier)
    translation += "%s    emlib.getConVar(%s, True).set(x)\n" %(indent, indexname)
    translation += "%s    emlib.getConVar(%s, True).set(%s)\n" %(indent, classname, identifier)

    new_command = __ParseLineCommand(translatorObj, args[5])

    for code in new_command:
        translation += translatorObj.translateParsedArgs(code.getLineno(), EssTranslator.parseArguments(code.getLine()), True, indent+'    ')

    return translation

def translate_foreach_part(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 6:
        raise EssSyntaxError(lineno, args, "foreach part <variable> <string> <character-count> <command>")

    varname = EssTranslator.expand(args[2], False, expanded)
    string = EssTranslator.expand(args[3], False, expanded)
    count = EssTranslator.expand(args[4], False, expanded)

    translation += "%si = 0\n" %(indent)
    translation += "%swhile i < len(%s):\n" %(indent, string)
    translation += "%s    emlib.getConVar(%s, True).set(%s[i:i+emlib.toInt(%s)])\n" %(indent, varname, string, count)
    translation += "%s    i += emlib.toInt(%s)\n" %(indent, count)
    
    new_command = __ParseLineCommand(translatorObj, args[5])

    for code in new_command:
        translation += translatorObj.translateParsedArgs(code.getLineno(), EssTranslator.parseArguments(code.getLine()), True, indent+'    ')

    return translation

def translate_foreach_token(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 6:
        raise EssSyntaxError(lineno, args, "foreach token <variable> <string> <separator> <command>")
    
    varname = EssTranslator.expand(args[2], False, expanded)
    string = EssTranslator.expand(args[3], False, expanded)
    separator = EssTranslator.expand(args[4], False, expanded)

    translation += "%sfor x in %s:\n" %(indent, string)
    translation += "%s    emlib.getConVar(%s, True).set(%s.split(%s))\n" %(indent, varname, string, separator)

    new_command = __ParseLineCommand(translatorObj, args[5])

    for code in new_command:
        translation += translatorObj.translateParsedArgs(code.getLineno(), EssTranslator.parseArguments(code.getLine()), True, indent+'    ')

    return translation 

def translate_foreach_weapon(translatorObj, lineno, args, expanded, indent):
    translation = ""

    if len(args) < 5:
        raise EssSyntaxError(lineno, args, "Syntax Error: foreach weapon <variable> <identifier> \"<command>\"")

    translatorObj.addImport("weaponlib\n")
    
    identifiers = args[3].replace('#hand', '#melee').replace('#nade', '#grenade')
    identifiers = args[3].replace('#', ',#')[1:]
    
    varname = EssTranslator.expand(args[2], False, expanded)
    identifiers = EssTranslator.expand(identifiers, False, expanded)

    translation += "%sweapons = list()\n" %indent
    translation += "%sfor weapon in %s.split(','):\n" %(indent, identifiers)
    translation += "%s    weapons.extend(weaponlib.getWeaponNameList(weapon))\n" % (indent)
    translation += "%swhile weapons:\n" % indent
    translation += "%s    weapon = weapons[0]\n" % indent
    translation += "%s    emlib.getConVar(%s, True).set(weapon)\n" %(indent, varname)

    new_command = __ParseLineCommand(translatorObj, args[4])

    for code in new_command:
        translation += translatorObj.translateParsedArgs(code.getLineno(), EssTranslator.parseArguments(code.getLine()), True, indent+'    ')

    translation += "%s    while weapon in weapons: weapons.remove(weapon)\n" %indent

    return translation   


command_callbacks = {
    "foreach"       : translate_foreach,
}


