import pytranslator
from pytranslator.pytranslator import EssTranslator
from pytranslator.pytranslator import EssSyntaxError

import re

###################################
#### Test operators description ###
###################################

TEST_OPERATORS_DESCRIPTION = {
#   "operator"       : (numericContext, pythonEquivalent) # numericContext is used to call EssTranslator.expand
    "equalto"        : (False, "=="), 
    "="              : (False, "=="), 
    "=="             : (False, "=="), 
    "notequalto"     : (False, "!="), 
    "!="             : (False, "!="), 
    "greaterthan"    : (True, ">"), 
    ">"              : (True, ">"), 
    "lessthan"       : (True, "<"), 
    "<"              : (True, "<"), 
    "notlessthan"    : (True, ">="), 
    ">="             : (True, ">="), 
    "=>"             : (True, ">="), 
    "notgreaterthan" : (True, "<="), 
    "<="             : (True, "<="), 
    "=<"             : (True, "<="), 
    ":"              : (False, "in"), 
    "in"             : (False, "in"), 
    "!:"             : (False, "not in"), # ES does not parse this operator correctly, it has to be quoted
    "notin"          : (False, "not in")
}

###################################
####### Callback definitions ######
###################################

# if (<loperande> <operator> <roperande>) then <commandeline>
# if (<loperande> <operator> <roperande>) do
def translate_if(translatorObj, lineno, args, expanded, indent):
#    translation = "# %s\n" % args
    translation = ""

    if len(args) < 6:
        raise EssSyntaxError(lineno, args, _("Too few arguments"))
    if (args[1] != "(") or (args[5] != ")"):
        raise EssSyntaxError(lineno, args, _("Missing parenthesis, or incorrect arguments"))

    expanded = expanded or (args[0] in expanding_commands_callbacks)

    blockbased = False
    if args[6] == "then":
        if len(args) < 7:
            raise EssSyntaxError(lineno, args, _("Missing command line to execute"))
    elif args[6] == "do":
        blockbased = True
    else:
        raise EssSyntaxError(lineno, args, _("Missing 'then' or 'do', or incorrect arguments"))
    
    op_description = TEST_OPERATORS_DESCRIPTION.get(args[3], None)
    if op_description is None:
        raise EssSyntaxError(lineno, args, "Unsupported operator '%s'" % args[3])
    op_numericContext, op_translation = op_description

    loperande = EssTranslator.expand(args[2], op_numericContext, expanded)
    roperande = EssTranslator.expand(args[4], op_numericContext, expanded)

    translation += "%sem.setFlag(%s %s %s, %s)\n" % (indent, loperande, op_translation, roperande, blockbased)
        
    if not blockbased:
        translation += "%sif em.getFlag(False):\n" % indent
        translation += translatorObj.translateParsedArgs(lineno, args[7:], expanded, "%s%s" % (indent, " "*4))

    return translation


# ifx true(<varname>) do
# ifx false(<varname>) do
# ifx parse(<varname>) do
def translate_ifx(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 5:
        raise EssSyntaxError(lineno, args, "Format is: ifx <mode>(<arguments>) do")
    if args[2] != "(" or args[4] != ")" or args[5] != "do":
        raise EssSyntaxError(lineno, args, "Format is: ifx <mode>(<arguments>) do")

    if args[1] == "true" or args[1] == "false":
        translation += "%sem.setFlag(%semlib.convarToBool(es.ServerVar(%s)), True)\n" \
            % (indent, {"true":"", "false":"not "}[args[1]], EssTranslator.expand(args[3], False, expanded))
    elif args[1] == "parse":
        translation += "%ses.mathparse('mathparse_result', %s)\n" % (indent, EssTranslator.expand(args[3], False, expanded))
        translation += "%sem.setFlag(bool(emlib.convar_ifxparse_result), True)\n" % indent
    else:
        raise EssSyntaxError(lineno, args, "ifx modes: true, false or parse")

    return translation


# else <commandline>
# else do
def translate_else(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 2:
        raise EssSyntaxError(lineno, args, "Format is: else <command line>")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)        

    if args[1].lower() == "do": # for this command, do is not case sensitive :-/
        translation += "%sem.swapFlag()\n" % indent
    else:
        translation += "%sif not em.getFlag(False):\n" % indent
        translation += translatorObj.translateParsedArgs(lineno, args[1:], expanded, "%s%s" % (indent, " "*4))

    return translation


###################################
###################################

# es <commandline>
def translate_es(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 2:
        raise EssSyntaxError(lineno, args, "Missing arguments")
    
    translation += translatorObj.translateParsedArgs(lineno, args[1:], True, indent)    
    
    return translation


def __expandAllArgs(args, expanded):
    """Expand all expandable args then returns a 'format % var' string"""
    # Prepare all arguments
    formatlist = []
    message = "'"
    for arg in args:
        expandedArg = EssTranslator.expand(arg, False, expanded)
        if expandedArg[1:len(expandedArg)-1] != arg:
            # This argument corresponds to a variable that has been expanded
            message +=  "%s "
            formatlist.append(expandedArg)
        else:
            message +=  "%s " % expandedArg[1:len(expandedArg)-1] # striping single quotes
    
    # Format the final message
    message = "%s'" % message[:len(message)-1]
    if len(formatlist) > 0:
        message += " % ("
        for formatarg in formatlist:
            message += "%s, " % formatarg
        message += ")"
        
    return message
    

# es_msg ...
def translate_es_msg(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 2:
        raise EssSyntaxError(lineno, args, "Missing arguments")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)        
    
    iArgs = 1
    if args[1].lower() == "#multi":
        iArgs += 1
    

    translation += "%ses.msg('#multi', %s)\n" % (indent, __expandAllArgs(args[iArgs:], expanded))
    
    return translation
    

# es_tell <userid> ...
def translate_es_tell(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_tell <userid> ...")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)        
        
    userid = EssTranslator.expand(args[1], False, expanded)
        
    iArgs = 2
    if args[2].lower() == "#multi":
        iArgs += 1
    
    translation += "%ses.tell(%s, '#multi', %s)\n" % (indent, userid, __expandAllArgs(args[iArgs:], expanded))
    
    return translation    
    
    
# es_centermsg ...
def translate_es_centermsg(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 2:
        raise EssSyntaxError(lineno, args, "Missing arguments")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)        
        
    translation += "%ses.centermsg(%s)\n" % (indent, __expandAllArgs(args[1:], expanded))
    
    return translation
    

# es_centertell <userid> ...
def translate_es_centertell(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_centertell <userid> ...")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)        
        
    userid = EssTranslator.expand(args[1], False, expanded)
        
    translation += "%ses.centertell(%s, %s)\n" % (indent, userid, __expandAllArgs(args[2:], expanded))
    
    return translation     
    
    
# es_dbgmsg <level> ...
def translate_es_dbgmsg(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_dbgmsg <level> ...")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)
        
    level = EssTranslator.expand(args[1], False, expanded)
        
    translation += "%ses.dbgmsg(%s, %s)\n" % (indent, level, __expandAllArgs(args[2:], expanded))
    
    return translation   
    
    
# es_dbgmsgv <level> <varname>
def translate_es_dbgmsgv(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_dbgmsgv <level> <varname>")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)
        
    level = EssTranslator.expand(args[1], False, expanded)
    varname = EssTranslator.expand(args[2], False, expanded)    
        
    translation += "%ses.dbgmsgv(%s, %s)\n" % (indent, level, varname)
    
    return translation         
    
    
# es_log ...
def translate_es_log(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 2:
        raise EssSyntaxError(lineno, args, "Missing arguments")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)        
    
    translation += "%ses.log(%s)\n" % (indent, __expandAllArgs(args[1:], expanded))
    
    return translation
   
    
# es_logv <varname>
def translate_es_logv(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 2:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_logv <varname>")

    expanded = expanded or (args[0] in expanding_commands_callbacks)
        
    varname = EssTranslator.expand(args[1], False, expanded)
        
    translation += "%ses.logv(%s)\n" % (indent, varname)
    
    return translation            
    
    
# es_set <varname> <value> [description]
def translate_es_set(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)
    
    if len(args) == 3:
        varname = EssTranslator.expand(args[1], False, expanded)
        value = EssTranslator.expand(args[2], False, expanded)
        translation += "%ses.set(%s, %s)\n" % (indent, varname, value)

    elif len(args) == 4:
        varname = EssTranslator.expand(args[1], False, expanded)
        value = EssTranslator.expand(args[2], False, expanded)    
        description = EssTranslator.expand(args[3], False, expanded)
        translation += "%ses.set(%s, %s, %s)\n" % (indent, varname, value, description)
        
    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_set <varname> <value> [description]")        
    
    return translation

    
# es_setinfo <varname> <value>
def translate_es_setinfo(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_setinfo <varname> <value>")            
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)        
        
    varname = EssTranslator.expand(args[1], False, expanded)
    value = EssTranslator.expand(args[2], False, expanded)
#    translation += "%ses.setinfo(%s, %s)\n" % (indent, varname, value)
    translation += "%ses.set(%s, %s)\n" % (indent, varname, value)
    
    return translation
    
    
# es_copy <dest> <src>
def translate_es_copy(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_copy <dest> <src>")            
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)        
        
    dest = EssTranslator.expand(args[1], False, expanded)
    src = EssTranslator.expand(args[2], False, expanded)
    translation += "%ses.copy(%s, %s)\n" % (indent, dest, src)
    
    return translation    


# es_forcevalue <varname> <value>
def translate_es_forcevalue(translateObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_forcevalue <varname> <value>")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)        
        
    varname = EssTranslator.expand(args[1], False, expanded)
    value = EssTranslator.expand(args[2], False, expanded)
    translation += "%ses.forcevalue(%s, %s)\n" % (indent, varname, value)
    
    return translation


# es_makepublic <varname>
def translate_es_makepublic(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 2:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_makepublic <varname>")  
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
        
    varname = EssTranslator.expand(args[1], False, expanded)
    translation += "%ses.makepublic(%s)\n" % (indent, varname)
    
    return translation    

    
# es_mathparse <varname> <expr>
def translate_es_mathparse(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_mathparse <varname> <path-expression>")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)        
    
    varname = EssTranslator.expand(args[1], False, expanded)
    expr = EssTranslator.expand(args[2], False, expanded)
    translation += "%ses.mathparse(%s, %s)\n" % (indent, varname, expr)    
    
    return translation  
    
    
# es_math <varname> <operator> [valeur]
def translate_es_math(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_math <varname> <operator> [value]")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)        
    
    varname = EssTranslator.expand(args[1], False, expanded)
    operator = args[2].lower()
    
    if len(args) == 3:        
        if operator in ("sin", "cos", "tan", "asin", "acos", "atan"):
            translatorObj.addImport("math")
            translation += "%semlib.getConVar(%s).set(math.%s(emlib.toFloat(emlib.getConVar(%s))))\n" % (indent, varname, operator, varname)
            
        elif operator in ("abs", "int", "float"):
            translation += "%semlib.getConVar(%s).set(%s(emlib.toFloat(emlib.getConVar(%s))))\n" % (indent, varname, operator, varname)            

        

        else:
            raise EssSyntaxError(lineno, args, "Unsupported operator '%s' for this arguments count" % operator)        
    
    elif len(args) == 4:
        value = EssTranslator.expand(args[3], False, expanded)

    
        if operator in ("+", "add"):
            translation += "%semlib.getConVar(%s).set(emlib.toFloat(emlib.getConVar(%s)) + emlib.toFloat(%s))\n" \
                % (indent, varname, varname, value)            

        elif operator in ("-", "substract"):
            translation += "%semlib.getConVar(%s).set(emlib.toFloat(emlib.getConVar(%s)) - emlib.toFloat(%s))\n" \
                % (indent, varname, varname, value)            

        elif operator in ("*", "multiply"):
            translation += "%semlib.getConVar(%s).set(emlib.toFloat(emlib.getConVar(%s)) * emlib.toFloat(%s))\n" \
                % (indent, varname, varname, value)            

        elif operator in ("/", "divide"):
            translation += "%semlib.getConVar(%s).set(emlib.toFloat(emlib.getConVar(%s)) / emlib.toFloat(%s))\n" \
                % (indent, varname, varname, value)            
                
        elif operator in ("=", "set"):
            translation += "%semlib.getConVar(%s).set(%s)\n" % (indent, varname, value) 
                
    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_math <varname> <operator> [value]")
    
    return translation    
      

# es_rand <varname> <min> <max>
def translate_es_rand(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_rand <varname> <min> <max>")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)        
    
    varname = EssTranslator.expand(args[1], False, expanded)
    minvalue = EssTranslator.expand(args[2], False, expanded)
    maxvalue = EssTranslator.expand(args[3], False, expanded)
    
    if (minvalue[1:len(minvalue)-1] == args[2]) and (maxvalue[1:len(maxvalue)-1] == args[2]): # known values or server_var? 
        if cmp(minvalue, maxvalue) != -1:
            raise EssSyntaxError(lineno, args, "<min> has to be greater than <max>")
            
    translatorObj.addImport("random")
    translation += "%semlib.getConVar(%s).set(random.randint(emlib.toInt(%s), emlib.toInt(%s)))\n" % (indent, varname, minvalue, maxvalue)
    
    return translation


# es_exists <varname> variable <value>
# es_exists <varname> map <value>
# es_exists <varname> saycommand <value>
# es_exists <varname> clientcommand <value>
# es_exists <varname> command <value>
# es_exists <varname> keygroup <value>
# es_exists <varname> key [keygroup] <value>
# es_exists <varname> keyvalue [keygroup] <key> <value>
# es_exists <varname> userid <value>
# es_exists <varname> script <value>
# es_exists <varname> block <value>
def translate_es_exists(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 4:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_exists <var> <type> [optional] [optional] <name-to-check>")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)
        
    varname = EssTranslator.expand(args[1], False, expanded)
#    operator = EssTranslator.expand(args[2], False, expanded)    
    
# Check if we know the operator
#    knownOp = False
#    if operator[1:len(operator)-1] == args[2]:
    # Yes
    if args[2] in ("variable", "map", "saycommand", "clientcommand", "command", "keygroup", "userid", "script", "block"):
#            knownOp = True
        if len(args) == 4:
            value = EssTranslator.expand(args[3], False, expanded)
            translation += "%semlib.getConVar(%s).set(es.exists('%s', %s))\n" % (indent, varname, args[2], value)
        else:
            raise EssSyntaxError(lineno, args, "Expected syntax: es_exists <var> <type> [optional] [optional] <name-to-check>")            
            
    elif args[2] == "key":
#            knownOp = True
        if len(args) == 4:
            value = EssTranslator.expand(args[3], False, expanded)
            translation += "%semlib.getConVar(%s).set(es.exists('key', %s))\n" % (indent, varname, value)
            
        elif len(args) == 5:
            keygroup = EssTranslator.expand(args[3], False, expanded)
            value = EssTranslator.expand(args[4], False, expanded)
            translation += "%semlib.getConVar(%s).set(es.exists('key', %s, %s))\n" % (indent, varname, keygroup, value)
            
        else:
            raise EssSyntaxError(lineno, args, "Expected syntax: es_exists <var> <type> [optional] [optional] <name-to-check>")                
            
    elif args[2] == "keyvalue":
#            knownOp = True
        if len(args) == 5:
            key = EssTranslator.expand(args[3], False, expanded)
            value = EssTranslator.expand(args[4], False, expanded)
            translation += "%semlib.getConVar(%s).set(es.exists('keyvalue', %s, %s))\n" % (indent, varname, key, value)
            
        elif len(args) == 6:
            keygroup = EssTranslator.expand(args[3], False, expanded)
            key = EssTranslator.expand(args[4], False, expanded)
            value = EssTranslator.expand(args[5], False, expanded)
            translation += "%semlib.getConVar(%s).set(es.exists('keyvalue', %s, %s, %s))\n" % (indent, varname, keygroup, key, value)
            
        else:
            raise EssSyntaxError(lineno, args, "Expected syntax: es_exists <var> <type> [optional] [optional] <name-to-check>")                                         
            
    else:
        raise EssSyntaxError(lineno, args, "Unsupported operator '%s'" % args[2])
                                
#    if not knownOp:
#        # No, operator resolved at runtime
#        if len(args) == 4:
#            value = EssTranslator.expand(args[3], False, expanded)
#            translation += "%semlib.getConVar(%s).set(es.exists(%s, %s))\n" % (indent, varname, operator, value)
#        elif len(args) == 5:
#            value1 = EssTranslator.expand(args[3], False, expanded)
#            value2 = EssTranslator.expand(args[4], False, expanded)            
#            translation += "%semlib.getConVar(%s).set(es.exists(%s, %s, %s))\n" % (indent, varname, operator, value1, value2)
#        elif len(args) == 6:
#            value1 = EssTranslator.expand(args[3], False, expanded)
#            value2 = EssTranslator.expand(args[4], False, expanded)            
#            value3 = EssTranslator.expand(args[5], False, expanded)                        
#            translation += "%semlib.getConVar(%s).set(es.exists(%s, %s, %s, %s))\n" % (indent, varname, operator, value1, value2, value3)
#        else:
#            raise EssSyntaxError(lineno, args, "Expected syntax: es_exists <var> <type> [optional] [optional] <name-to-check>")                                                     
    
    return translation


# es_format <varname> <format> [token1 [token2 [token3 [token4 [token5 [token6 [token7 [token8 [token9]]]]]]]]]
def translate_es_format(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_format <varname> <format> [token1 [token2 [token3 [token4 [token5 [token6 [token7 [token8 [token9]]]]]]]]]")
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)
    
    varname = EssTranslator.expand(args[1], False, expanded)
    format = EssTranslator.expand(args[2], False, expanded)
    
    # Replace all %*n* by %s
    TOKEN_PATTERN = re.compile("%[1-9]")
    for target in TOKEN_PATTERN.finditer(format):
        format = format.replace(target.group(), "%s")
    
    translation = "%semlib.getConVar(%s).set(%s %% (" % (indent, varname, format)
    for token in args[3:]:
        translation += "%s, " % EssTranslator.expand(token, False, expanded)
    translation += "))\n"
    
    return translation
    
    
# es_formatv <varname> <format> [tokenvar1 [tokenvar2 [tokenvar3 [tokenvar4 [tokenvar5 [tokenvar6 [tokenvar7 [tokenvar8 [tokenvar9]]]]]]]]]
def translate_es_formatv(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_formatv <varname> <format> [tokenvar1 [tokenvar2 [tokenvar3 [tokenvar4 [tokenvar5 [tokenvar6 [tokenvar7 [tokenvar8 [tokenvar9]]]]]]]]]")
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)
    
    varname = EssTranslator.expand(args[1], False, expanded)
    format = EssTranslator.expand(args[2], False, expanded)
    
    # Replace all %*n* by %s
    TOKEN_PATTERN = re.compile("%[1-9]")
    for target in TOKEN_PATTERN.finditer(format):
        format = format.replace(target.group(), "%s")
    
    translation = "%semlib.getConVar(%s).set(%s %% (" % (indent, varname, format)
    for token in args[3:]:
        translation += "str(emlib.getConVar(%s)), " % EssTranslator.expand(token, False, expanded)
    translation += "))\n"
    
    return translation    
    
    

# es_string <varname> <operator> <value1> [value2]
def translate_es_string(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)    
    
    if len(args) == 4:
        varname = EssTranslator.expand(args[1], False, expanded)
        operator = args[2]
        value1 = EssTranslator.expand(args[3], False, expanded)
        
        if operator == "replace":
            translation += "%semlib.getConVar(%s).set(str(emlib.getConVar(%s)).replace(%s, ''))\n" % (indent, varname, varname, value1)
            
        elif operator == "replacev":
            translation += "%semlib.getConVar(%s).set(str(emlib.getConVar(%s)).replace(str(emlib.getConVar(%s)), '')\n" % (indent, varname, varname, value1)
                
        else:
            raise EssSyntaxError(lineno, args, "Unsupported operator for this arguments count")        
        
    elif len(args) == 5:
        varname = EssTranslator.expand(args[1], False, expanded)
        operator = args[2]
        value1 = EssTranslator.expand(args[3], False, expanded)
        value2 = EssTranslator.expand(args[4], False, expanded) 

        if operator == "replace":
            translation += "%semlib.getConVar(%s).set(str(emlib.getConVar(%s)).replace(%s, %s))\n" % (indent, varname, varname, value1, value2)
            
        elif operator == "replacev":
            translation += "%semlib.getConVar(%s).set(str(emlib.getConVar(%s)).replace(str(emlib.getConVar(%s)), str(emlib.getConVar(%s))))\n" \
                 % (indent, varname, varname, value1, value2)
                
        elif operator == "section":
            translation += "%svalue = str(emlib.getConVar(%s))\n" % (indent, varname)            
            translation += "%sstart = emlib.toInt(%s)\n" % (indent, value1)
            translation += "%send = emlib.toInt(%s)\n" % (indent, value2)       
            translation += "%sif start >= 0 and start <= end <= len(value):\n" % indent
            translation += "%s    emmlib.getConVar(%s).set(value[start:end])\n" % (indent, varname)
            translation += "%selse:\n" % indent
            translation += "%s    es.dbgmsg(0, '%s l.%s: Unable to get section %%s %%s in string \'%%s\'' %% (start, end, value))\n" % \
                (indent, translatorObj.getScript().getName(), lineno)
        
        else:
            raise EssSyntaxError(lineno, args, "Unsupported operator '%s'" % operator)
    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_string <varname> <operator> <value1> [value2]")        
            
    return translation          
  
  
# es_strcmp <varname> <string1> <string2>
def translate_es_strcmp(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 4:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_strcmp <varname> <string1> <string2>")
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)
    
    varname = EssTranslator.expand(args[1], False, expanded)
    string1 = EssTranslator.expand(args[2], False, expanded)
    string2 = EssTranslator.expand(args[3], False, expanded)        
    
    translation += "%semlib.getConVar(%s).set(cmp(%s, %s))\n" % (indent, varname, string1, string2)
    
    return translation
    
        
# es_strlen <varname> <string>
def translate_es_strlen(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_strlen <varname> <string>")
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)
    
    varname = EssTranslator.expand(args[1], False, expanded)
    string = EssTranslator.expand(args[2], False, expanded)
    
    translation += "%semlib.getConVar(%s).set(len(%s))\n" % (indent, varname, string)
    
    return translation        
    
    
# es_token <varname> <string> <token> [separator]
def translate_es_token(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) in (4, 5):
        varname = EssTranslator.expand(args[1], False, expanded)
        string = EssTranslator.expand(args[2], False, expanded)
        token = EssTranslator.expand(args[3], False, expanded)
        
        translation += "%stokens = %s.split()\n" % (indent, string)
        translation += "%si = emlib.toInt(%s)\n" % (indent, token)
        translation += "%sif 0 <= i < len(tokens):\n" % indent
        translation += "%s    emlib.getConVar(%s).set(str(tokens[i])\n" % (indent, varname)
        translation += "%selse:\n" % indent
        translation += "%s    es.dbgmsg(0, '%s l.%s: Token %s not found in string \'%s\'')\n" % \
            (indent, translatorObj.getScript().getName(), lineno, token, string)
    
    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_token <varname> <string> <token> [separator]")
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)
    
    return translation        
        

# es_regcmd <commandname> <script>/<block> [description]
def translate_es_regcmd(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)    
    
    if len(args) == 3:
        cmdname = EssTranslator.expand(args[1], False, expanded)
        block =  EssTranslator.expand(args[2], False, expanded)    
        translation += "%ses.regcmd(%s, %s)\n" % (indent, cmdname, block)
        
    elif len(args) == 4:
        cmdname = EssTranslator.expand(args[1], False, expanded)
        block =  EssTranslator.expand(args[2], False, expanded)    
        description =  EssTranslator.expand(args[3], False, expanded)            
        translation += "%ses.regcmd(%s, %s, %s)\n" % (indent, cmdname, block, description)

    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_regcmd <commandname> <script>/<block> [description]")                                                     
        
    return translation
    


# es_regsaycmd <commandname> <script>/<block> [description]
def translate_es_regsaycmd(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)    
    
    if len(args) == 3:
        cmdname = EssTranslator.expand(args[1], False, expanded)
        block =  EssTranslator.expand(args[2], False, expanded)    
        translation += "%ses.regsaycmd(%s, %s)\n" % (indent, cmdname, block)
        
    elif len(args) == 4:
        cmdname = EssTranslator.expand(args[1], False, expanded)
        block =  EssTranslator.expand(args[2], False, expanded)    
        description =  EssTranslator.expand(args[3], False, expanded)            
        translation += "%ses.regsaycmd(%s, %s, %s)\n" % (indent, cmdname, block, description)

    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_regcmd <commandname> <script>/<block> [description]")                                                     
        
    return translation    
    

# es_unregsaycmd <commandname>
def translate_es_unregsaycmd(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 2:
        raise EssSyntaxError(lineno, args, "es_unregsaycmd <commandname>")
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)  
    
    cmdname = EssTranslator.expand(args[1], False, expanded)
    
    translation += "%ses.unregsaycmd(%s)\n" % (indent, cmdname)    
        
    return translation      


# es_regclientcmd <commandname> <script>/<block> [description]
def translate_es_regclientcmd(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)    
    
    if len(args) == 3:
        cmdname = EssTranslator.expand(args[1], False, expanded)
        block =  EssTranslator.expand(args[2], False, expanded)    
        translation += "%ses.regclientcmd(%s, %s)\n" % (indent, cmdname, block)
        
    elif len(args) == 4:
        cmdname = EssTranslator.expand(args[1], False, expanded)
        block =  EssTranslator.expand(args[2], False, expanded)    


        description =  EssTranslator.expand(args[3], False, expanded)            
        translation += "%ses.regclientcmd(%s, %s, %s)\n" % (indent, cmdname, block, description)

    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_regclientcmd <commandname> <script>/<block> [description]")                                                     
        
    return translation    
    
    
# es_unregclientcmd <commandname>
def translate_es_unregclientcmd(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 2:
        raise EssSyntaxError(lineno, args, "es_unregclientcmd <commandname>")
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)  
    
    cmdname = EssTranslator.expand(args[1], False, expanded)
    
    translation += "%ses.unregclientcmd(%s)\n" % (indent, cmdname)    
        
    return translation        
    

# es_getargv <varname> <index>
def translate_es_getargv(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_getargv <varname> <index>")                                                     
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
    
    varname = EssTranslator.expand(args[1], False, expanded)
    index = EssTranslator.expand(args[2], False, expanded)    
    translation += "%semlib.getConVar(%s).set(es.getargv(%s))\n" % (indent, varname, index)
    
    return translation
    
    
# es_getargc <varname>
def translate_es_getargc(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 2:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_getargc <varname>")                                                     
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
    
    varname = EssTranslator.expand(args[1], False, expanded)
    translation += "%semlib.getConVar(%s).set(es.getargc())\n" % (indent, varname)
    
    return translation    


# es_getargs <varname>
def translate_es_getargs(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 2:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_getargs <varname>")                                                     
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
    
    varname = EssTranslator.expand(args[1], False, expanded)
    translation += "%semlib.getConVar(%s).set(es.getargs())\n" % (indent, varname)
    
    return translation    


# es_getcmduserid <varname>
def translate_es_getcmduserid(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 2:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_getcmduserid <varname>")                                                     
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
    
    varname = EssTranslator.expand(args[1], False, expanded)
    translation += "%semlib.getConVar(%s).set(es.getcmduserid())\n" % (indent, varname)
    
    return translation  
    
    
# es_cexec <userid> ...
def translate_es_cexec(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_cexec <userid> <commandline>")                                                     
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
    
    userid = EssTranslator.expand(args[1], False, expanded)
    cmdline = __expandAllArgs(args[2:], expanded)
    translation += "%ses.cexec(%s, %s)\n" % (indent, userid, cmdline)
    
    return translation     
    
    
# es_cexec_all ...
def translate_es_cexec_all(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 2:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_cexec_all <commandline>")                                                     
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
    
    cmdline = __expandAllArgs(args[1:], expanded) 
    translation += "%ses.cexec_all(%s)\n" % (indent, cmdline)
    
    return translation           


# es_sexec <userid> ...
def translate_es_sexec(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_sexec <userid> <commandline>")                                                     
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
    
    userid = EssTranslator.expand(args[1], False, expanded)
    cmdline = __expandAllArgs(args[2:], expanded) 
    translation += "%ses.sexec(%s, %s)\n" % (indent, userid, cmdline)
    
    return translation          
            
            
# es_sexec_all ...
def translate_es_sexec_all(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 2:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_sexec_all <commandline>")                                                     
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
    
    cmdline = __expandAllArgs(args[1:], expanded)
    translation += "%ses.sexec_all(%s)\n" % (indent, cmdline)
    
    return translation     
    
    
# es_commandv <varname>
def translate_es_commandv(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 2:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_commandv <varname>")                                                     
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
    
    varname = EssTranslator.expand(args[1], False, expanded)    
    translation += "%ses.commandv(%s)\n" % (indent, varname)
    
    return translation     
       
       
# es_changeteam <userid> <team>
def translate_es_changeteam(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_changeteam <userid> <team>")                                                     
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
    
    userid = EssTranslator.expand(args[1], False, expanded)
    team = EssTranslator.expand(args[2], False, expanded)    
    translation += "%ses.changeteam(%s, %s)\n" % (indent, userid, team)
    
    return translation   
                    
    
# es_getuserid <varname> [info]
def translate_es_getuserid(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)    
    
    if len(args) == 2:
        varname =  EssTranslator.expand(args[1], False, expanded)
        translation += "%semlib.getConVar(%s).set(es.getuserid())\n" % (indent, varname)

    elif len(args) == 3:
        varname = EssTranslator.expand(args[1], False, expanded)
        info = EssTranslator.expand(args[2], False, expanded)        
        translation += "%semlib.getConVar(%s).set(es.getuserid(%s))\n" % (indent, varname, info)
                
    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_getuserid <varname> [info]")  
        
    return translation
        
    
# es_getplayername <varname> <userid>
def translate_es_getplayername(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_getplayername <varname> <userid>")  
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            

    varname = EssTranslator.expand(args[1], False, expanded)
    userid = EssTranslator.expand(args[2], False, expanded)    
    translation += "%semlib.getConVar(%s).set(es.getplayername(%s))\n" % (indent, varname, userid)
        
    return translation
    

# es_getplayerteam <varname> <userid>
def translate_es_getplayerteam(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_getplayerteam <varname> <userid>")  
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            

    varname = EssTranslator.expand(args[1], False, expanded)
    userid = EssTranslator.expand(args[2], False, expanded)    
    translation += "%semlib.getConVar(%s).set(es.getplayerteam(%s))\n" % (indent, varname, userid)
        
    return translation
        
       
# es_getplayersteamid <varname> <userid>
def translate_es_getplayersteamid(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_getplayersteamid <varname> <userid>")  
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            

    varname = EssTranslator.expand(args[1], False, expanded)
    userid = EssTranslator.expand(args[2], False, expanded)    
    translation += "%semlib.getConVar(%s).set(es.getplayersteamid(%s))\n" % (indent, varname, userid)
        
    return translation
            

# es_getclientvar <varname> <userid> <clientvarname>
def translate_es_getclientvar(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_getclientvar <varname> <userid> <clientvarname>")  
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            

    varname = EssTranslator.expand(args[1], False, expanded)
    userid = EssTranslator.expand(args[2], False, expanded)    
    clientvarname = EssTranslator.expand(args[3], False, expanded)     
    translation += "%semlib.getConVar(%s).set(es.getclientvar(%s, %s))\n" % (indent, varname, userid, clientvarname)
        
    return translation
     
    
# es_botsetvalue <userid> <varname> <value>
def translate_es_botsetvalue(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 4:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_botsetvalue <userid> <varname> <value>")  
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            

    userid =  EssTranslator.expand(args[1], False, expanded)    
    varname =  EssTranslator.expand(args[2], False, expanded)
    value =  EssTranslator.expand(args[3], False, expanded)    
    translation += "%ses.botsetvalue(%s, %s, %s))\n" % (indent, userid, varname, value)
        
    return translation    
            
      
# es_load <addon>
def translate_es_load(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 2:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_load <addon-script>")  
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
        
    addon = EssTranslator.expand(args[1], False, expanded)
    translation += "%ses.load(%s)\n" % (indent, addon)
    
    return translation    
    
    
# es_unload <addon>
def translate_es_unload(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 2:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_unload <addon-script>")  
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
        
    addon = EssTranslator.expand(args[1], False, expanded)
    translation += "%ses.unload(%s)\n" % (indent, addon)
    
    return translation   
        

# es_reload <addon>
def translate_es_reload(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 2:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_reload <addon-script>")  
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
        
    addon = EssTranslator.expand(args[1], False, expanded)
    translation += "%ses.reload(%s)\n" % (indent, addon)
    
    return translation   
            

# es_doblock <script>/<block>
def translate_es_doblock(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 2:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_doblock <script>/<block>")  
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
        
    block = EssTranslator.expand(args[1], False, expanded)
    
    # Check if the block is one of our function
    pyfunction = False
    BLOCK_PATTERN = re.compile("^([^/]+/[^/]+)$")
    if BLOCK_PATTERN.match(args[1]) is not None:
        scriptname, blockname = args[1].split("/")
        script = translatorObj.getScript()

        if scriptname == script.getName():
            if blockname in (block.getName() for block in script.getBlocks()):
               pyfunction = True
               translation += "%s%s()\n" % (indent, blockname)    
            else:
                raise EssSyntaxError(lineno, args, "Unknown block")

    if not pyfunction:
         translation += "%ses.doblock(%s)\n" % (indent, block)
    
    return translation    

    
# es_keygroupcreate <name>
def translate_es_keygroupcreate(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 2:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_keygroupcreate <groupname>")  
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
        
    groupname = EssTranslator.expand(args[1], False, expanded)
    translation += "%ses.keygroupcreate(%s)\n" % (indent, groupname)    
    
    return translation
    
  
# es_keygroupdelete <groupname>
def translate_es_keygroupdelete(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 2:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_keygroupdelete <groupname>")  
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
        
    groupname = EssTranslator.expand(args[1], False, expanded)
    translation += "%ses.keygroupdelete(%s)\n" % (indent, groupname)    
    
    return translation
    
    
# es_keygroupcopy <src> <dest>
def translate_es_keygroupcopy(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_keygroupcopy <src> <dest>")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)              
        
    src = EssTranslator.expand(args[1], False, expanded)
    dest = EssTranslator.expand(args[2], False, expanded)    
    translation += "%ses.keygroupcopy(%s, %s)\n" % (indent, src, dest)    
    
    return translation    


# es_keygroupfilter [keygroup] <not/only> <keyvalue> <value>
def translate_es_keygroupfilter(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)    
    
    if len(args) == 4:
        operator = EssTranslator.expand(args[1], False, expanded)
        
        if operator[1:len(operator)-1] == args[1]: # Operator not resolved at runtime
            if args[1] not in ("not", "only"):
                raise EssSyntaxError(lineno, args, "Unsupported operator")
        
        keyvalue = EssTranslator.expand(args[2], False, expanded)
        value =  EssTranslator.expand(args[3], False, expanded)
        translation += "%ses.keygroupfilter(%s, %s, %s)\n" % (indent, operator, keyvalue, value)
            
    elif len(args) == 5:
        keygroup = EssTranslator.expand(args[1], False, expanded)
        operator = EssTranslator.expand(args[2], False, expanded)
        
        if operator[1:len(operator)-1] == args[2]: # Operator not resolved at runtime
            if args[1] not in ("not", "only"):
                raise EssSyntaxError(lineno, args, "Unsupported operator")
        
        keyvalue = EssTranslator.expand(args[3], False, expanded)
        value =  EssTranslator.expand(args[4], False, expanded)
        translation += "%ses.keygroupfilter(%s, %s, %s, %s)\n" % (indent, keygroup, operator, keyvalue, value)
    
    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_keygroupfilter [keygroup] <not/only> <keyvalue> <value>")  
        
    
    return translation    


# es_keygroupload <keygroup> [path]
def translate_es_keygroupload(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)    
    
    if len(args) == 2:
        keygroup = EssTranslator.expand(args[1], False, expanded)
        translation += "%ses.keygroupload(%s)\n" % (indent, keygroup)    
    
    elif len(args) == 3:
        keygroup = EssTranslator.expand(args[1], False, expanded)
        path = EssTranslator.expand(args[2], False, expanded)
        translation += "%ses.keygroupload(%s, %s)\n" % (indent, keygroup, path)    
            
    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_keygroupload <keygroup> [path]")  
    
    return translation    
       

# es_keygroupmsg <userid> <mode> <keygroup>
def translate_es_keygroupmsg(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 4:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_keygroupmsg <userid> <type> <keygroup>")  
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
        
    userid = EssTranslator.expand(args[1], False, expanded)
    mode = EssTranslator.expand(args[1], False, expanded)
    keygroup = EssTranslator.expand(args[1], False, expanded)
    
    translation += "%ses.keygroupmsg(%s, %s, %s)\n" % (indent, userid, mode, keygroup)    
    
    return translation    
        
        
# es_keygrouprename <keygroup> <newname>
def translate_es_keygrouprename(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) != 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_keygrouprename <keygroup> <newname>")  

    expanded = expanded or (args[0] in expanding_commands_callbacks)    
        
    keygroup = EssTranslator.expand(args[1], False, expanded)
    newname = EssTranslator.expand(args[2], False, expanded)    
    translation += "%ses.keygrouprename(%s, %s)\n" % (indent, keygroup, newname)    
    
    return translation        


# es_keygroupsave <keygroup> [path]
def translate_es_keygroupsave(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)    
    
    if len(args) == 2:
        keygroup = EssTranslator.expand(args[1], False, expanded)
        translation += "%ses.keygroupsave(%s)\n" % (indent, keygroup)    
    
    elif len(args) == 3:
        keygroup = EssTranslator.expand(args[1], False, expanded)
        path = EssTranslator.expand(args[2], False, expanded)
        translation += "%ses.keygroupsave(%s, %s)\n" % (indent, keygroup, path)    
            
    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_keygroupsave <keygroup> [path]")  
    
    return translation    
            
        
# es_keylist [keygroup]
def translate_es_keylist(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)    
    
    if len(args) == 1:
        translation += "%ses.keylist()\n" % indent
    
    elif len(args) == 2:
        keygroup = EssTranslator.expand(args[1], False, expanded)
        translation += "%ses.keylist(%s)\n" % (indent, keygroup)    
            
    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_keylist [keygroup]")  
    
    return translation    


# es_keyrename [keygroup] <key> <newname>
def translate_es_keyrename(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)    
    
    if len(args) == 3:
        key = EssTranslator.expand(args[1], False, expanded)    
        newname = EssTranslator.expand(args[2], False, expanded)    
        translation += "%ses.keyrename(%s, %s)\n" % (indent, key, newname)            
    
    elif len(args) == 4:
        keygroup = EssTranslator.expand(args[1], False, expanded)
        key = EssTranslator.expand(args[2], False, expanded)    
        newname = EssTranslator.expand(args[3], False, expanded)    
        translation += "%ses.keyrename(%s, %s, %s)\n" % (indent, keygroup, key, newname)            
                        
    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_keyrename [keygroup] <key> <newname>")  
    
    return translation  

    
# es_keycreate [groupname] <name>
def translate_es_keycreate(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)    
    
    if len(args) == 2:
        keyname = EssTranslator.expand(args[1], False, expanded)
        translation += "%ses.keycreate(%s)\n" % (indent, keyname)    
    
    elif len(args) == 3:
        groupname =  EssTranslator.expand(args[1], False, expanded)
        keyname = EssTranslator.expand(args[2], False, expanded)
        translation += "%ses.keycreate(%s, %s)\n" % (indent, groupname, keyname)
        
    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_keycreate [groupname] <groupname>")  
        
    return translation    
   
   
# es_keydelete [groupname] <name>
def translate_es_keydelete(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)    
    
    if len(args) == 2:
        keyname = EssTranslator.expand(args[1], False, expanded)
        translation += "%ses.keydelete(%s)\n" % (indent, keyname)    
    
    elif len(args) == 3:
        groupname =  EssTranslator.expand(args[1], False, expanded)
        keyname = EssTranslator.expand(args[2], False, expanded)
        translation += "%ses.keydelete(%s, %s)\n" % (indent, groupname, keyname)
        
    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_keydelete [groupname] <groupname>")  
        
    return translation
        

# es_keysetvalue [groupname] <key> <keyvalue> <value>
def translate_es_keysetvalue(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)    
    
    if len(args) == 4:
        keyname = EssTranslator.expand(args[1], False, expanded)
        keyvalue = EssTranslator.expand(args[2], False, expanded)
        value = EssTranslator.expand(args[3], False, expanded)                
        translation += "%ses.keysetvalue(%s, %s, %s)\n" % (indent, keyname, keyvalue, value)
        
    elif len(args) == 5:
        keygroup =  EssTranslator.expand(args[1], False, expanded)
        keyname = EssTranslator.expand(args[2], False, expanded)
        keyvalue = EssTranslator.expand(args[3], False, expanded)
        value = EssTranslator.expand(args[4], False, expanded)                
        translation += "%ses.keysetvalue(%s, %s, %s, %s)\n" % (indent, keygroup, keyname, keyvalue, value)            
        
    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_keysetvalue [groupname] <key> <keyvalue> <value>")  
    
    return translation


# es_keygetvalue <varname> [groupname] <key> <keyvalue>
def translate_es_keygetvalue(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    expanded = expanded or (args[0] in expanding_commands_callbacks)    
    
    if len(args) == 4:
        varname = EssTranslator.expand(args[1], False, expanded)
        keyname = EssTranslator.expand(args[2], False, expanded)
        keyvalue = EssTranslator.expand(args[3], False, expanded)
        translation += "%ses.keygetvalue(%s, %s, %s)\n" % (indent, varname, keyname, keyvalue)
        
    elif len(args) == 5:
        varname = EssTranslator.expand(args[1], False, expanded)
        keygroup =  EssTranslator.expand(args[2], False, expanded)
        keyname = EssTranslator.expand(args[3], False, expanded)
        keyvalue = EssTranslator.expand(args[4], False, expanded)      
        translation += "%ses.keygetvalue(%s, %s, %s, %s)\n" % (indent, varname, keygroup, keyname, keyvalue)            
        
    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_keygetvalue <varname> [groupname] <key> <keyvalue>")
    
    return translation
    

# es_foreachkey <varname> in <groupname> <commandline>
def translate_es_foreachkey(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if (len(args) != 5) or (args[2].lower() != "in"):
        raise EssSyntaxError(lineno, args, "Expected syntax: es_foreachkey <varname> in [groupname] <key> <commandline>")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)        

    varname = EssTranslator.expand(args[1], False, expanded)    
    keygroup = EssTranslator.expand(args[3], False, expanded)
    cmdline = EssTranslator.expand(args[4], False, expanded)
    translation += "%ses.foreachkey(%s, 'in', %s, %s)\n" % (indent, varname, keygroup, cmdline)
         
    return translation
    
    
# es_foreachval <varname> in [groupname] <key> <commandline>
def translate_es_foreachval(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if args[2].lower() != "in":
        raise EssSyntaxError(lineno, args, "Expected syntax: es_foreachval <varname> in [groupname] <key> <commandline>")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            
    
    if len(args) == 5:
        varname = EssTranslator.expand(args[1], False, expanded)
        keyname = EssTranslator.expand(args[3], False, expanded)
        cmdline = EssTranslator.expand(args[4], False, expanded)
        translation += "%ses.foreachval(%s, 'in', %s, %s)\n" % (indent, varname, keyname, cmdline)
        
    elif len(args) == 6:
        varname = EssTranslator.expand(args[1], False, expanded)    
        keygroup = EssTranslator.expand(args[3], False, expanded)
        keyname = EssTranslator.expand(args[4], False, expanded)
        cmdline = EssTranslator.expand(args[5], False, expanded)
        translation += "%ses.foreachval(%s, 'in', %s, %s, %s)\n" % (indent, varname, keygroup, keyname, cmdline)
                
    else:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_foreachval <varname> in [groupname] <key> <commandline>")
    
    return translation
    
    
# es_setplayerprop <userid> <property> <value>
def translate_es_setplayerprop(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 4:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_setplayerprop <userid> <property> <value>")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)  
    
    userid = EssTranslator.expand(args[1], False, expanded)
    prop = EssTranslator.expand(args[2], False, expanded)
    value = EssTranslator.expand(args[3], False, expanded)    
    translation += "%ses.setplayerprop(%s, %s, %s)\n" % (indent, userid, prop, value)
    
    return translation


# es_getplayerprop <varname> <userid> <property>
def translate_es_getplayerprop(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 4:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_getplayerprop <varname> <userid> <property>")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)  

    varname = EssTranslator.expand(args[1], False, expanded)
    userid = EssTranslator.expand(args[2], False, expanded)
    prop = EssTranslator.expand(args[3], False, expanded)
    translation += "%semlib.getConVar(%s).set(es.getplayerprop(%s, %s))\n" % (indent, varname, userid, prop)
    
    return translation
    
    
# es_delayed <delay> <commandline>
def translate_es_delayed(translatorObj, lineno, args, expanded, indent):
    translation = ""

    if len(args) < 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: es_delayed <delay> <commandline>")
        
    expanded = expanded or (args[0] in expanding_commands_callbacks)            

    delay = EssTranslator.expand(args[1], False, expanded)
    translation += "%ses.delayed(%s, '%s')\n" % (indent, delay, " ".join(arg for arg in args[2:]))
    
    return translation    
   
        

###################################
###### Callback declarations ######
###################################

# commands having an es_x equivalent
expanding_commands_callbacks = {
    "if"                     : translate_if, 
    "else"                   : translate_else,  
    
    ###################################
    ################################### 
    "es_msg"                 : translate_es_msg, 
    "es_tell"                : translate_es_tell, 
    "es_centermsg"           : translate_es_centermsg, 
    "es_centertell"          : translate_es_centertell,         
    "es_dbgmsg"              : translate_es_dbgmsg,    
    "es_dbgmsgv"             : translate_es_dbgmsgv,        
    "es_log"                 : translate_es_log,        
#    "es_logq"                : translate_es_logq,      
    "es_logv"                : translate_es_logv,              
           
    "es_set"                 : translate_es_set,    
    "es_setinfo"             : translate_es_setinfo,    
    "es_copy"                : translate_es_copy, 
    "es_forcevalue"          : translate_es_forcevalue,
    "es_makepublic"          : translate_es_makepublic, 
    "es_mathparse"           : translate_es_mathparse,               
    "es_math"                : translate_es_math, 
    "es_rand"                : translate_es_rand, 
    "es_exists"              : translate_es_exists,  
    "es_format"              : translate_es_format, 
    "es_formatv"             : translate_es_formatv, 
#    "es_formatqv"              : translate_es_formatv,            
    "es_string"              : translate_es_string, 
    "es_strcmp"              : translate_es_strcmp,    
    "es_strlen"              : translate_es_strlen,      
    "es_token"               : translate_es_token,          
    
    "es_regcmd"              : translate_es_regcmd,    
    "es_regsaycmd"           : translate_es_regsaycmd, 
    "es_regclientcmd"        : translate_es_regclientcmd,    
    "es_unregsaycmd"         : translate_es_unregsaycmd, 
    "es_unregclientcmd"      : translate_es_unregclientcmd,     
    "es_getargv"             : translate_es_getargv,  
    "es_getargc"             : translate_es_getargc, 
    "es_getargs"             : translate_es_getargs,    
    "es_getcmduserid"        : translate_es_getcmduserid, 
    "es_cexec"               : translate_es_cexec, 
    "es_cexec_all"           : translate_es_cexec_all, 
    "es_sexec"               : translate_es_sexec, 
    "es_sexec_all"           : translate_es_sexec_all, 
    "es_commandv"            : translate_es_commandv, 
    
    "es_changeteam"          : translate_es_changeteam, 
            
    "es_getuserid"           : translate_es_getuserid, 
    "es_getplayername"       : translate_es_getplayername, 
    "es_getplayerteam"       : translate_es_getplayerteam,    
    "es_getplayersteamid"    : translate_es_getplayersteamid,        
    "es_getclientvar"        : translate_es_getclientvar,    
    
    "es_botsetvalue"         : translate_es_botsetvalue,    
    
    "es_load"                : translate_es_load,  
    "es_unload"              : translate_es_unload,  
    "es_reload"              : translate_es_reload,          
    
    "es_doblock"             : translate_es_doblock, 
    
    "es_keygroupcreate"      : translate_es_keygroupcreate,    
    "es_keygroupdelete"      : translate_es_keygroupdelete,     
    "es_keygroupcopy"        : translate_es_keygroupcopy,    
    "es_keygroupfilter"      : translate_es_keygroupfilter, 
    "es_keygroupload"        : translate_es_keygroupload, 
    "es_keygroupmsg"         : translate_es_keygroupmsg,    
    "es_keygrouprename"      : translate_es_keygrouprename, 
    "es_keygroupsave"        : translate_es_keygroupsave,    
    "es_keycreate"           : translate_es_keycreate,    
    "es_keydelete"           : translate_es_keydelete,        
    "es_keylist"             : translate_es_keylist,            
    "es_keyrename"           : translate_es_keyrename,               
    "es_keysetvalue"         : translate_es_keysetvalue, 
    "es_keygetvalue"         : translate_es_keygetvalue,  
    "es_foreachkey"          : translate_es_foreachkey,  
    "es_foreachval"          : translate_es_foreachval,          
    
#    "es_give"               : translate_es_give, 
#    "es_fire"               : translate_es_fire, 
#    "es_remove"             : translate_es_remove, 
#    "es_entcreate"          : translate_es_entcreate, 
#    "es_mexec"              : translate_es_mexec, 
#    "es_prop_physics_create": translate_es_prop_physics_create, 
#    "es_prop_dynamic_create": translate_es_prop_dynamic_create, 
#    "es_setpos"             : translate_es_setpos, 
#    "es_setang"             : translate_es_setang, 
#    "es_trick"              : translate_es_trick, 

    "es_setplayerprop"      : translate_es_setplayerprop, 
    "es_getplayerprop"      : translate_es_getplayerprop, 
    
    "es_delayed"        : translate_es_delayed, 
}

# All commands
command_callbacks = {
    "ifx"           : translate_ifx, 
    "es"            : translate_es, 
}
# Add es_x commands variante in command_callbacks
for command in expanding_commands_callbacks:
    command_callbacks[command] = expanding_commands_callbacks[command]
    command_callbacks["es_x%s" % (command[3:] if command.startswith("es_") else command)] = expanding_commands_callbacks[command]

