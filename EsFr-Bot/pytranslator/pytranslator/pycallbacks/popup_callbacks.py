import pytranslator
from pytranslator.pytranslator import EssTranslator
from pytranslator.pytranslator import EssSyntaxError

me = __import__(__name__)

def translate_popup(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 2:
        raise EssSyntaxError(lineno, args, "Expected syntax: popup <subcommand> [args...]")
    
    translatorObj.add_import("popuplib")
    
    subcommand = EssTranslator.expand(args[1], False, expanded)
    
    callback_name = "translator_popup_%s" % subcommand
    if hasattr(me, callback_name):
        getattr(me, callback_name)(translatorObj, lineno, args, expanded, indent)
    else:
        # unsupported subcommand
        translation += "%ses.server.insertcmd('%s" % (indent, parsedArgs[0])
        for arg in parsedArgs[1:]:
            translation += " %s" % arg
        translation += "')\n"
    
    return translation
    
    
def translate_popup_create(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: popup create <popup id>")
        
    popupid = EssTranslator.expand(args[2], False, expanded)
    translation += "%spopuplib.create(%s)\n" % (indent, popupid)    
    
    return translation

    
def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 3:
        raise EssSyntaxError(lineno, args, "Expected syntax: popup delete <popup id>")
        
    popupid = EssTranslator.expand(args[2], False, expanded)
    translation += "%spopuplib.delete(%s)\n" % (indent, popupid)    
    
    return translation

        
def translate_popup_delline(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 4:
        raise EssSyntaxError(lineno, args, "Expected syntax: popup addline <popup id> <line number>")
        
    popupid = EssTranslator.expand(args[2], False, expanded)
    line = EssTranslator.expand(args[3], False, expanded)
    #translation += "%spopuplib.delline(%s, emlib.toInt(%s))\n" % (indent, popupid, line)    
    
    return translation

        
def translate_popup_addline(translatorObj, lineno, args, expanded, indent):
    translation = ""
    
    if len(args) < 4:
        raise EssSyntaxError(lineno, args, "Expected syntax: popup addline <popup id> <text>")
        
    popupid = EssTranslator.expand(args[2], False, expanded)
    text = EssTranslator.expand(args[3], False, expanded)    
    #translation += "%spopuplib.addline(%s, %s)\n" % (indent, popupid, text)    
    
    return translation

    
# def translate_popup_insline(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_modline(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_addlinef(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_inslinef(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_modlinef(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_prepuser(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_menuselect(translatorObj, lineno, args, expanded, indent):
#     pass


# def translate_popup_menuselectfb(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_select(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_submenu(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_menuvalue(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_timeout(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_send(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def  translate_popup_unsend(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_unsendid(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_unsendname(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_close(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass

    
# def translate_popup_delete(translatorObj, lineno, args, expanded, indent):
#     pass



command_callbacks = {
    "popup"      : translate_popup, 
}
