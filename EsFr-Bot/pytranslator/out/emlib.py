"""Help functions used by generated code"""

import es

class ExecutionManager(object):
    '''Manage the execution of the translated ESShell code'''

    def __init__(self):
        # Last test was True?
        self.__testFlag = False
        
        # Last block-based test was True?
        self.__blockFlag = False
    
    def setFlag(self, value, blockbased):
        '''Update the test flag
        blockbased : Is the block flag must be updated too?
        '''
        self.__testFlag = value
        if blockbased:
            self.__blockFlag = value
          
    def swapFlag(self):
        '''Swap the block flag only'''
        self.__blockFlag = not self.__blockFlag
        
    def getFlag(self, blockbased):
        '''Get the test flag 
        blockbased : Do the block flag must be returned instead?
        '''
        return self.__blockFlag if blockbased else self.__testFlag

em = ExecutionManager()

# Output ConVar for mathparse calls
convar_ifxparse_result = es.ServerVar('mathparse_result', '')

# Determine if the convars have to exists before use
convar_eventscripts_autocreate = es.ServerVar('eventscripts_autocreate')


class FakeServerVar(es.ServerVar):
    '''Fake ConVar'''
    
    def __str__(self):
        return '0.0'
        
    def __float__(self):
        return 0.0
    
    def __int__(self):
        return 0
    
    def __nonzero__(self):
        return False
    
    def set(self, value):
        pass
            
    def copy(self, source):
        pass
    
    def makepublic(self):
        pass
        
    def addFlag(self, flagname):
        pass
        
    def removeFlag(self, flagname):
        pass
    
    def forcecallbacks(self):
        pass
        
# Fake ConVar used in getConvar()
convar_fake = FakeServerVar("fake")        


def isFloat(value):
    '''Return True if value is a float, False otherwise
    value : string value to test
    '''
    result = None
    try:
        result = float(value)
    except ValueError:
        pass
    return result is not None


def getConVar(name):
    '''Return a convar instance ("0" if the convar does not exist)
    name : The convar's name
    '''
    result = convar_fake

    if int(convar_eventscripts_autocreate):
        result = es.ServerVar(name)
    else:
        if es.exists("variable", name):
            result = es.ServerVar(name)
        else:
            es.dbgmsg(0,"WARNING: Couldn't find server variable: %s" % name)

    return result
        

def convarToBool(convar):
    '''Empty string / "0" / 0 => False, others => True
    convar : convar to test
    '''
    result = str(convar)
    if isFloat(result):
        result = float(result)
    return bool(result)


def toFloat(value):
    '''Convert a value to float
    If the string can't be converted, return 0.0
    value : string value to convert
    '''
    result = 0.0
    try:
        result = float(value)
    except ValueError:
        es.dbgmsg(0, "WARNING: Non-numeric value used as a numeric value: %s" % name)
    return result    
    
def toInt(value):
    '''Convert a value to integer
    If the string can't be converted, return 0
    value : string value to convert
    '''
    result = 0
    try:
        result = int(value)
    except ValueError:
        es.dbgmsg(0, "WARNING: Non-numeric value used as a numeric value: %s" % name)
    return result        
    

