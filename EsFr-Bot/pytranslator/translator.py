from __future__ import with_statement
import sys, os, timeit, codecs

import locale, gettext

# I18n
#try:
#    # Try to find pytranslator.mo with the current locale
#    locale_dir = os.path.join(os.path.dirname(sys.argv[0]), "resources/locale")
#    locale_lang = locale.getdefaultlocale()[0][:2]
#    gettext.translation("pytranslator", locale_dir, [locale_lang]).install(True)
#except IOError, e:
#    # File not found
#    print e
#    import __builtin__
#    __builtin__.__dict__["_"] = lambda text: text # defines _
import __builtin__
__builtin__.__dict__["_"] = lambda text: text


SCRIPT_PATH = os.path.join(os.path.dirname(sys.argv[0]), "")
sys.path.append(os.path.join(SCRIPT_PATH, "parsers"))

import ess.esspreproc as esspreproc
import ess.esslexer as esslexer
import ess.essparser as essparser
import output.consoleoutput as consoleoutput
import pytranslator.pytranslator as translator
import pytranslator.pycallbacks as callbacks

def main():
    argc = len(sys.argv)

    if argc == 1:
        print "%s script-to-convert" % sys.argv[0]
        
    else:
        filepath = sys.argv[1] 
        debug = argc > 2 and int(sys.argv[2])
        output = consoleoutput.ConsoleOutput()
        
        try:
            # ESS to code object
            preproc = esspreproc.EssPreProcessor(output)
            lexer = esslexer.EssLexer(output, debug)
            parser = essparser.EssParser(output, debug, lexer)
      
            with codecs.open(filepath, "r", "utf-8-sig") as f:
                code = preproc.parse(filepath, f.read(), debug)
                if debug:
                    with open("out/%s.preproc" % filepath,"w") as preprocfile:
                        preprocfile.write(code)
                
                print _("Parsing ESS...")
                codeObj = parser.parse(filepath, code, debug)
                print "%s errors, %s warnings" % (output.errorCount(), output.warningCount())
                print

                # code object to ESP
                if codeObj is not None:
                    if debug:
                        with open("out/%s.memory" % filepath, "w") as objfile:
                            objfile.write(repr(codeObj))

                    print _("Generating PY...")
                    translateObj = translator.EssTranslator(output)
                    for callback in callbacks.command_callbacks: # install callbacks for the known commands
                        translateObj.addCallback(callback, callbacks.command_callbacks[callback])

                    translateObj.translate(codeObj)
                    print "%(errorcount)s errors" % {"errorcount" : len(translateObj.getErrors())}
               
                print
                print "Done",
        except IOError,e:
            sys.stderr.write("%s\n" % e)

if __name__ == "__main__":
    print "\n%s" % (_("Execution time: %(time).2f seconds") % {"time" : timeit.Timer("main()", "from __main__ import main").timeit(1)})
    
