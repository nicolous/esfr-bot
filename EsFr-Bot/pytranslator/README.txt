translator.py:
    L'application : analyse du code ESS, puis appel du générateur de code Python.
    
parsers/:
    Les parsers ESShell et KeyValues.
    
pytranslator/:
    pytranslator.py:
        Générateur de code Python.
        
    pycallbacks/:
        Définition des fonctions qui traduisent les commandes ESS en code Python.
        Chaque module doit définir un dictionnaire "command_callbacks" avec pour clés le nom d'une commande ESS 
         et pour valeur la fonction Python à appeler pour traduire cette commande.
        Si un nouveau module est ajouté, il faut le déclarer dans le fichier __init__.py du même dossier.
        
    resources/:
        emlib.py:
            Fonctions à fournir avec le code généré.
