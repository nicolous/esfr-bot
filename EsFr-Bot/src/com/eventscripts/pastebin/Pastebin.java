package com.eventscripts.pastebin;


public class Pastebin
{
    private String url;
    
    private String code;
    
    private String parent;

    public Pastebin(String url, String code)
    {
        this.url = url;
        this.code = code;
    }
    
    public String getUrl()
    {
        return url;
    }
    
    public String getId()
    {
        String id = "";
        
        int iId = url.lastIndexOf('/');
        if (iId != -1)
            id = url.substring(iId + 1);
        
        return id;
    }
    
    public String getCode()
    {
        return code;
    }
    
    public String getParent()
    {
        return parent;
    }
    
    public void setParent(String parent)
    {
        this.parent = parent;
    }
}
