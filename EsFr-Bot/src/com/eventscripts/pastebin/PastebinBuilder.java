package com.eventscripts.pastebin;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.jibble.pircbot.PircBot;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class PastebinBuilder
{
    private PircBot bot;
    
    private String website;
    
    public PastebinBuilder(PircBot bot, String website)
    {
        this.bot = bot;
        this.website = website;
    }
    
    public String getWebsite()
    {
        return website;
    }
    
    public Pastebin getPastebin(String url) throws IOException, ParserConfigurationException, SAXException
    {
        Pastebin paste = null;
        
        HttpURLConnection httpcon = (HttpURLConnection)new URL(url).openConnection(); 
        httpcon.setConnectTimeout(5000);
        httpcon.setReadTimeout(5000);
        
        DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
        DocumentBuilder constructeur = fabrique.newDocumentBuilder();

        Document document = constructeur.parse(httpcon.getInputStream());
        Element eCode = document.getElementById("code");
        if (eCode != null)
        {
            String code = eCode.getTextContent();
            if (code != null)
            {
                code = new String(code.getBytes(document.getXmlEncoding()), "UTF-8");
            }
            paste = new Pastebin(url, code);
        }
        
        return paste;
    }
    
    private Pastebin newPastebin(String parent, String format, String code) throws IOException
    {
       Pastebin newPastebin = null;
        
        String code2 = URLEncoder.encode(code, "UTF-8");
        StringBuilder request = new StringBuilder();
        request.append("parent_pid=").append(parent)
               .append("&format=").append(format)
               .append("&code2=").append(code2)
               .append("&secret=80")
               .append("&expiry=d")
               .append("&poster=").append(bot.getNick())
               .append("&paste=Send");
        
        HttpURLConnection post = (HttpURLConnection)new URL(website).openConnection();
        post.setRequestMethod("POST");
        post.setDoOutput(true);
        post.setDoInput(true);
        post.setUseCaches(false);        
        post.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
 
        DataOutputStream s = new DataOutputStream(post.getOutputStream());
        s.writeBytes(request.toString());
        s.flush();
        s.close();
        
        Pattern urlRes = Pattern.compile("/pastebin\\.php\\?dl=([0-9]+)"); 
        BufferedReader reader = new BufferedReader(new InputStreamReader(post.getInputStream())); 
        String line = reader.readLine();
        while (line != null)
        {
            Matcher m = urlRes.matcher(line); 
            if (m.find())
            {
                newPastebin = new Pastebin(website + "/" + m.group(1), code);
            }
            line = reader.readLine();
        }
        
        return newPastebin;        
    }
    
    public Pastebin newPastebin(String format, String code) throws IOException
    {
        return newPastebin("", format, code);
    }
    
    public Pastebin newCode(Pastebin pastebin, String format, String code) throws IOException
    {
        return newPastebin(pastebin.getId(), format, code);
    }
}
