/*
 * Copyright 2008-2011 Nicolas Maingot
 * 
 * This file is part of EsFr-Bot.
 *
 * EsFr-Bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * EsFr-Bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EsFr-Bot.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.eventscripts.search;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.eventscripts.pircbot.EsFrBot;

/** 
 * Outil de recherche sur google
 */
public class GoogleSearchTask extends Thread
{
    /** Nombre maximum de résultats affichés par le bot pour éviter le flood */ 
    private int limitResults;
    
    /** Référence sur le bot effectuant la recherche */
    private EsFrBot bot;
    
    /** Mots-clés de la recherche */
    private String searchString;
    
    /** Rechercher parmi les pages francophones uniquement */
    private boolean frenchOnly;
    
    /** L'auteur de la recherche */
    private String recipient;
    
    /** Prépare la recherche 
     * 
     * @param esfrBot Référence sur le bot qui fait la recherche
     * @param keywords Les mots-clés de la recherche, séparés par des espaces
     * @param french Est-ce que la recherche porte sur des pages françaises uniquement
     * @param resultRecipient La cible qui recevra les résultats
     * @param maxResults Nombre maximum de résultats affichés
     */
    public GoogleSearchTask(EsFrBot esfrBot, String keywords, boolean french, String resultRecipient, int maxResults)
    {
        bot = esfrBot;
        searchString = keywords.replaceAll("\\s+", "+");
        frenchOnly = french;
        recipient = resultRecipient;
        limitResults = maxResults;
    }
    
    @Override
    public void run()
    {
        try
        {
            String urlString = "http://www.google.fr/search?hl=fr&rlz=1B3GGGL_frFR234FR236&q=" + searchString + "&btnG=Recherche+Google"; 
            if (frenchOnly)
                urlString += "&tbs=lr:lang_1fr&lr=lang_fr"; 
            URL url = new URL(urlString);
            
            HttpURLConnection httpcon = (HttpURLConnection)url.openConnection(); 
            httpcon.setConnectTimeout(5000);
            httpcon.setReadTimeout(5000);
            
            // On se fait passer pour un navigateur pour les filtres anti-bot
            httpcon.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; fr; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 GTB5 (.NET CLR 3.5.30729)");
            httpcon.addRequestProperty("Referer", "http://www.google.fr/");
            //System.out.println(httpcon.getRequestProperties());
            
            //Pattern patternProp = Pattern.compile("class=\"r\"><a href=\"/url\\?q=[^\"]+\"");
            Pattern pattern = Pattern.compile("class=\"r\"><a href=\"http[^\"]+\"");
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpcon.getInputStream()));
            String line = reader.readLine();
            int nbResults = 0;
            boolean stop = false;
            while(line != null && (! stop))
            {
                //System.out.println(line);
                /*Matcher maProp = patternProp.matcher(line);
                while(maProp.find() && (! stop))
                {
                    nbResults++;
                    
                    String result = maProp.group();
                    bot.sendMessage(recipient, result.substring(26, result.indexOf("&")));

                    if (nbResults == limitResults)
                        stop = true;
                }*/

                Matcher ma = pattern.matcher(line);
                while(ma.find() && (! stop))
                {
                    nbResults++;
                    
                    String result = ma.group();
                    bot.sendMessage(recipient, result.substring(19, result.length()-1));
                    
                    if (nbResults == limitResults)
                        stop = true;
                }
           
                line = reader.readLine();
            }
                        
            if (nbResults == 0)
                bot.safeMessage(recipient, "Aucun résultat");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            bot.safeMessage(recipient, "Erreur rencontrée durant la recherche (site indisponible ou bot non à-jour).");
        }            
    }
}
