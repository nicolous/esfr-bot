/*
 * Copyright 2008-2011 Nicolas Maingot
 * 
 * This file is part of EsFr-Bot.
 *
 * EsFr-Bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * EsFr-Bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EsFr-Bot.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.eventscripts.discussion;

import com.eventscripts.pircbot.EsFrBot;

/** 
 * Donne du répondant au bot :-P 
 */
public class DefaultConversation
{
    /** Le bot concerné */
    protected EsFrBot bot;
    
    /** Date d'envoie du message */
    private long sendDate;
    
    /**
     * 
     * @param theBot Le bot qui a vu le message
     */
    public DefaultConversation(EsFrBot theBot)
    {
        bot = theBot;
        sendDate = 0;
    }
    
    /** Le bot rejoint un channel
     * 
     * @param channel Le nom du channel
     */
    public void greetings(String channel)
    {
    }
    
    /** Le bot quitte un channel
     * 
     * @param channel Le nom du channel
     */
    public void bye(String channel)
    {
    }
    
    /** Le bot se déconnecte du serveur */
    public void disconnect()
    {
    }
    
    /** Analyse un message
     * @param sender L'auteur du message
     * @param channel Le channel où le message a été écrit
     * @param senderIsOp Si l'auteur est opérateur
     * @param text Le message
     */
    public void parseText(String sender, boolean senderIsOp, String channel, String text)
    {
    }
    
    /** Simule une saisie au clavier (joue sur la date d'envoi du message) 
     * @param delay Le délai avant de commencer à taper le message
     * @param target Le destinataire du message
     * @param message Le message
     */
    public void typeMessage(long delay, String target, String message)
    {
        long realDelay = delay + message.length() * 1000 / 6;
        long currentDate = System.currentTimeMillis();
        
        if (currentDate < sendDate)
            realDelay += sendDate - currentDate;

        sendDate = currentDate + realDelay;
        bot.delayedMessage(realDelay, target, message);
    }
}
