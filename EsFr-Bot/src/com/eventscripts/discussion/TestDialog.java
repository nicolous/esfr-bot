/*
 * Copyright 2008-2011 Nicolas Maingot
 * 
 * This file is part of EsFr-Bot.
 *
 * EsFr-Bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * EsFr-Bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EsFr-Bot.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.eventscripts.discussion;

import java.util.Calendar;
import java.util.Random;
import java.util.regex.Pattern;

import com.eventscripts.pircbot.EsFrBot;


/** 
 * Permet au bot de discuter avec un utilisateur au milieu d'autres discussions 
 */
public class TestDialog extends DefaultConversation
{    
    private Random rand;
    
    //private String interlocutor;
    
    private String lastMsg;
    
       private static Pattern sep = 
            Pattern.compile("\\.|\\!|\\?|;|,|-");
       
    private static Pattern negation = 
        Pattern.compile("\\b(n'|pas?|jamais)\\b");
    
    private static Pattern love = 
        Pattern.compile("\\b(bel(le)?|sex.*|joli.*|mignon(ne)?|sympa.*|gentil.*|.*adore|canon|érotique|.*aime|<3|m'épouser|bi(s|z)ou(s|x)?)\\b");
    
    private static Pattern dis = 
        Pattern.compile("\\b(t(a ?)?g(ueuelle)?|dégage|moche|pas?\\sbel(le)?|.*aime pas?|.*emmerde|(du)?con(ne)?|salope|pute|pétasse|cochon(ne)|cul?cul?)\\b");    
    
    private static Pattern hello = 
        Pattern.compile("\\b(hi|hello|salu.*|lut?|re|b.*jour|b.*soir|coucou)\\b");
    
    private static Pattern bye = 
        Pattern.compile("\\b(bye|(@|a|à)\\s*(\\+|plu(s|ch)).*|b(onne)?\\s*n(uit)?|au\\s*revoir)\\b");
    
    private static Pattern feel = 
        Pattern.compile("\\b((ç|c)a va|comm?ent?.+vas?|bien)\\b");
    
    private static Pattern daddy = 
        Pattern.compile("\\b(créateu?r|auteur|p(é|è)re|papa|marr?ie?|frère|s(oe|œ)ur)\\b");
    
    private static Pattern language = 
        Pattern.compile("\\b(faite?|écri(te?|s)?|langu?(ag)e|program?m?ée?)\\b");
    
    private static Pattern age = 
        Pattern.compile("\\b((â|a)ge)\\b");
    
    private static Pattern living = 
        Pattern.compile("\\b(habites?|vie|crèche|chez\\stoi|origine)\\b");

    /*private static Pattern lame = 
        Pattern.compile("\\b(lamm?e(u?r)?|hack((eu?r)|(age))?|(rés(eaux?)|o)|prog(ramm(ation)|(er))?|codeu?r|c(\\+\\+)?|v(isual )?b(asic)?|java(script)?|html|cs:?s|plugin|serveu?r|as(sembleur|m)|x86|rcon(_password)?|failles?(fix)?|ironwall|pij(ama)?|alex(7170)?|sql|php|site|web|informati(que|cien)|offset|virtuel(le)?|mémoire|ping|crash|plant(age|é)|srcds|héxa(décimal)?)\\b");
    */
    
    public TestDialog(EsFrBot theBot)
    {
        super(theBot);
        
        rand = new Random();
        lastMsg = new String();
        //interlocutor = new String();
    }
    
    @Override
    public void greetings(String channel)
    {
        Calendar calendar = Calendar.getInstance();
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        if (hours >= 5 && hours <= 17)
            typeMessage(0, channel, "Bonjour tout le monde !");
        else
            typeMessage(0, channel, "Bonsoir tout le monde !");
    }
    
    @Override
    public void bye(String channel)
    {
        Calendar calendar = Calendar.getInstance();
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        if (hours >= 5 && hours <= 17)
            typeMessage(0, channel, "Bonne journée à tous !");
        else if (hours > 17 && hours <= 22)
            typeMessage(0, channel, "Bonne soirée à tous !");
        else
            typeMessage(0, channel, "Bonne nuit tout le monde !");
    }
    
    @Override
    public void parseText(String sender, boolean senderIsOp, String channel, String text)
    {
        String message = text.toLowerCase();
        if (! message.equalsIgnoreCase(lastMsg))
        {
            lastMsg = message;
            
            String botNick = bot.getNick().toLowerCase();
            
            // Quelqu'un écrit le nom du bot
            if (message.matches("([^a-z]*\\s+)?" + botNick + "(\\s+[^a-z]*)?"))
            {                
                iAmHere(sender, senderIsOp, channel, message);
                //interlocutor = sender;                
            }
            
            // Quelqu'un parle au/du bot
            else if (speakWithMe(sender, channel, message))
            {
                //interlocutor = sender;
                String [] phrases = message.split(sep.pattern());
                
                for (String phrase : phrases)
                {
                    if (hello.matcher(phrase).find())
                    hi(sender, senderIsOp, channel, phrase);
                    
                    else if (dis.matcher(phrase).matches())
                    {
                        if (negation.matcher(phrase).find())
                            loveYou(sender, senderIsOp, channel, phrase);
                        else
                            hateYou(sender, senderIsOp, channel, phrase);
                    }
                    
                    else if (bye.matcher(phrase).find())
                        bye(sender, senderIsOp, channel, phrase);
                    
                    else if (feel.matcher(phrase).find())
                        howIFeel(sender, senderIsOp, channel, phrase);
                    
                    else if (daddy.matcher(phrase).find())
                        whoCreatedMe(sender, senderIsOp, channel, phrase);
                    
                    else if (language.matcher(phrase).find())
                        typeMessage(0, channel, "J'ai été écrit par Nicolous en Java grâce à l'API Pircot (http://www.jibble.org/pircbot.php).");
                    
                    else if (age.matcher(phrase).find())
                        howOld(sender, senderIsOp, channel, phrase);
                    
                    else if (living.matcher(phrase).find())
                        whereLiving(sender, senderIsOp, channel, phrase);
                    
                    else if (love.matcher(phrase).find())
                    {
                        if (negation.matcher(phrase).find())
                            hateYou(sender, senderIsOp, channel, phrase);
                        else
                            loveYou(sender, senderIsOp, channel, phrase);
                    }
                    
                    else
                        talk(sender, senderIsOp, channel, phrase);
                }
            }
            /*else if (lame.matcher(message).find())
            {
                DateFormat format = new SimpleDateFormat("EEEE");
                String jour = format.format(new Date());
                
                String lamerbotName = "LamerDu" + jour.substring(0,1).toUpperCase() + jour.substring(1);
                
                if (rand.nextInt(101) <= 25)
                {
                    User lamerbot = null;
                    for (User user : bot.getUsers(channel))
                    {
                        if (user.getNick().equals(lamerbotName))
                        {
                            lamerbot = user;
                            break;
                        }
                    }
                   
                    if (lamerbot == null)
                        bot.sendRawLineViaQueue("PRIVMSG " + lamerbotName + " :!cmd join " + channel);
                }
            }*/
            /*else
            {
                interlocutor = "";
            }*/
        }
    }
    
    private boolean speakWithMe(String sender, String channel, String message)
    {
       boolean ok = true;
        
        /*if (sender.equals(interlocutor))
        {
            for (User user : bot.getUsers(channel))
            {
                if (message.contains(user.getNick()))
                {
                    ok = false;
                    break;
                }
            }
        }
        else*/
            ok = message.matches(".*\\b(" + bot.getNick().toLowerCase() + ")\\b.*");
        
        return ok;
    }
    
    private void hi(String sender, boolean senderIsOp, String channel, String phrase)
    {
        if (rand.nextBoolean())
        {
            Calendar calendar = Calendar.getInstance();
            int hours = calendar.get(Calendar.HOUR_OF_DAY);
            if (hours >= 5 && hours <= 17)
                typeMessage(0, channel, "Bonjour " + sender + " !");
            else
                typeMessage(0, channel, "Bonsoir " + sender + " !");
        }
        else
            typeMessage(0, channel, "Salut " + sender + " !");        
    }
    
    private void bye(String sender, boolean senderIsOp, String channel, String phrase)
    {
        if (rand.nextBoolean())
        {
            Calendar calendar = Calendar.getInstance();
            int hours = calendar.get(Calendar.HOUR_OF_DAY);
            if (hours >= 5 && hours <= 17)
                typeMessage(0, channel, "Bonne journée " + sender + " !");
            else if (hours > 17 && hours <= 22)
                typeMessage(0, channel, "Bonne soirée " + sender + " !");
            else
                typeMessage(0, channel, "Bonne nuit " + sender + " !");
        }
        else
            typeMessage(0, channel, "@++ " + sender + " !");     
    }    
    
    private void iAmHere(String sender, boolean senderIsOp, String channel, String phrase)
    {
        String [] answers = {
                "Je suis là !",
                "C'est moi !",
                "Oui ?",
                "?",
                "C'est elle",
                "Quoi ? :-|",
                "Raconte moi tout :)",
                "Plop !",
                sender + " ?",
                "Oui, " + sender + " ?"};
        
        typeMessage(0, channel, answers[rand.nextInt(answers.length)]);        
    }
    
    private void loveYou(String sender, boolean senderIsOp, String channel, String phrase)
    {
        String [] answers = {
                "Merci !",
                "Cherche pas je suis trop bien pour toi.",
                "C'est gentil !",
                "Tu sais la seule chose féminine en moi, c'est mon pseudo...",
                "Dans une autre vie peut-être :-)"};
        
        typeMessage(0, channel, answers[rand.nextInt(answers.length)]);            
    }
    
    private void hateYou(String sender, boolean senderIsOp, String channel, String phrase)
    {
        if (senderIsOp)
        {
            String [] answers = {
                    "Oui, mon maître :|",
                    "Si tu savais ce que je pense de toi...",
                    "C'est pas gentil :(",
                    "Gouja ! >:-(",
                    "Je te crotte.",
                    ";-("};
            
            typeMessage(0, channel, answers[rand.nextInt(answers.length)]);
        }
        else
        {
            String [] answers = {
                    "Gouja ! >:-(",
                    "Sympa...",
                    "Va voir ailleurs si j'y suis !",
                    "Mon pseudo c'est pas pigeon hein !",
                    "Un autre volontaire ??  >:-(",
                    ";-("};
            
            if (! (channel.equals(sender)))
                bot.kick(channel, sender, answers[rand.nextInt(answers.length)]);
        }         
    }    
    
    private void howIFeel(String sender, boolean senderIsOp, String channel, String phrase)
    {
        String [] answers = {
                "Tranquille :)",
                "Tout va pour le mieux !",
                "Ca va merci, et toi ?",
                "Je vais toujours bien :-D",
                "Je ne suis pas sûre que l'on puisse parler de santé avec moi :-|",
                "J'ai froid.",
                "Ça gaze :-D"};
        
        typeMessage(0, channel, answers[rand.nextInt(answers.length)]);       
    }
    
    private void whoCreatedMe(String sender, boolean senderIsOp, String channel, String phrase)
    {
        String [] answers = {
                "D'une certaine manière, Nico est mon père O_O",
                "Nico <3",
                "Et bien, si l'on accepte le fait qu'un programme puisse être enfanté, Nico est mon père...",
                "J'ai été programmé par Nico :-)",
                "Je suis le fruit d'un geek !!",
                "Mon papa m'a dit de ne pas adresser la parole aux gens que je connais pas >:-D"};
        
        typeMessage(0, channel, answers[rand.nextInt(answers.length)]);        
    }
    
    private void howOld(String sender, boolean senderIsOp, String channel, String phrase)
    {
        String [] answers = {
                "J'ai 5 ans et j'habite encore chez mes parents espèce de gros pervers !",
                "Demande à Nicolous :-|",
                "Franchement, tu crois vraiment que je peux vieillir ?",
                "Le seule chose dont je suis sûre, c'est que je peux vivre plus longtemps que toi :-P",
                "Je suis trop jeune pour toi de toute façon."};
        
        typeMessage(0, channel, answers[rand.nextInt(answers.length)]);        
    }
    
    private void whereLiving(String sender, boolean senderIsOp, String channel, String phrase)
    {
        String [] answers = {
                "En face d'un bloc en silicium...",
                "0x00C49288 ?",
                "Je sais pas, on m'a pas demandé mon avis :-/",
                "Non mais tu ne m'intéresses pas laisse tomber.",
                "Je suis plus prêt que tu ne le crois :-P"};
        
        typeMessage(0, channel, answers[rand.nextInt(answers.length)]);        
    }    
    
    private void talk(String sender, boolean senderIsOp, String channel, String phrase)
    {
        String [] answers = {
                "J'ai po compris :-|",
                "Tu parles trop " + sender + " ^^",
                "Il y a de la friture sur la ligne, j'ai du mal à te comprendre :-S",
                "Salut, je ne suis pas là pour le moment, veuillez laisser un message après le bip sonore.",
                sender + " ?",
                "?",
                "Ok",
                "D'accord...",
                "Je ne sais pas",
                "Débrouille-toi",
                "C'est pas mon problème",
                "Ben... c'est cool",
                "Mais qu'est-ce qu'il dit ? O_o",
                "C'est à moi que tu parles ?",
                "Cherche un peu plus, je suis sûre que tu vas trouver...",
                "Demande à Google : http://www.google.fr/search?hl=fr&q=" + phrase.replace(" ","+"),
                "Tu peux développer STP ?",
                "Et ?",                
                "Hu ?!",
                "Oui",
                "Sûr ?",
                "Tu mens",
                "Pas sûr",
                "Je suis occupée",
                "Ben voyons",
                "Le numéro que vous avez composé n'est plus attribué, veuillez consulter le service des renseignements.",
                "118 ?",
                "Changeons de sujet :-)",
                "J'attends la suite...",
                "Mais encore ?",
                "Qui sait...",
                "Cours toujours !",
                "Attends, je consulte ma boule de cristal :-|",
                "Je suis à cours d'idée !"};
        
        typeMessage(0, channel, answers[rand.nextInt(answers.length)]);        
    }
}
