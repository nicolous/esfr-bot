/*
 * Copyright 2008-2011 Nicolas Maingot
 * 
 * This file is part of EsFr-Bot.
 *
 * EsFr-Bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * EsFr-Bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EsFr-Bot.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.eventscripts.esam;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.jibble.pircbot.Colors;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.eventscripts.pircbot.EsFrBot;


/** Recherche et affiche la liste des scripts contenant le mot-clé donné
 * @see http://forums.mattie.info/cs/forums/viewtopic.php?t=21983
 */
public class AddonSearchTask extends Thread 
{
    /** Nombre maximum de résultats affichés */ 
    public static int maxResults = 5;
    
    /** Tous les types de recherche possibles */
    public static enum SearchTypeCode {ADDON, NAME, DESC, USER, PVAR, ESMIN}
    
    /** Correspondance {code type => type} */
    private HashMap<SearchTypeCode, String> searchTypes;
    
    /** Référence sur le bot effectuant la recherche */
    private EsFrBot bot;
    
    /** Mot clé de la recherche */
    private String searchString;
    
    /** Type de recherche à effectuer */
    private SearchTypeCode searchTypeCode;
    
    /** Destinataire des résultats */
    private String recipient;
    
    /** Prépare la recherche
     * 
     * @param esfrBot Référence sur le bot qui fait la recherche
     * @param keyword Le mot clé de la recherche
     * @param typeCode Le type de recherche effectué
     * @param recipient Le pseudo de l'auteur de la recherche
     */
    public AddonSearchTask(EsFrBot esfrBot, String keyword, SearchTypeCode typeCode, String searchAuthor)
    {
        searchTypes = new HashMap<SearchTypeCode, String>();
        searchTypes.put(SearchTypeCode.ADDON, "addon");
        searchTypes.put(SearchTypeCode.NAME, "name");
        searchTypes.put(SearchTypeCode.DESC, "desc");
        searchTypes.put(SearchTypeCode.USER, "user");
        searchTypes.put(SearchTypeCode.PVAR, "pvar");
        searchTypes.put(SearchTypeCode.ESMIN, "esmin");
        
        bot = esfrBot;
        searchString = keyword;
        searchTypeCode = typeCode;
        recipient = searchAuthor;
    }
    
    public void run()
    {
        try
        {
            URL url = new URL("http://addons.eventscripts.com/sdk/search/" + searchString + "/" + searchTypes.get(searchTypeCode));

            URLConnection connection = url.openConnection();
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);        
            InputStream stream = connection.getInputStream();
            
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
           
            Document document = builder.parse(stream);
            String encoding = document.getXmlEncoding();
            
            // Affichage des résultats :
            NodeList results = document.getElementsByTagName("addon");
            int nbrResults = results.getLength();
            int i = 0;
            if (nbrResults == 0)
                bot.safeMessage(recipient, "Aucun résultat.");
            else
            {
                if (nbrResults > maxResults)
                {
                    nbrResults = maxResults;
                    bot.safeMessage(recipient, "NB : Plus de résultats sur http://addons.eventscripts.com/addons/search/" + searchString);
                }
                do
                {
                    Element addon = (Element)results.item(i);
                    
                    String addonName = new String(addon.getElementsByTagName("addon_basename").item(0).getTextContent().getBytes(encoding), "UTF-8");
                    String addonAuthor = new String(addon.getElementsByTagName("addon_author").item(0).getTextContent().getBytes(encoding), "UTF-8");
                    String addonSummary = new String(addon.getElementsByTagName("addon_summary").item(0).getTextContent().getBytes(encoding), "UTF-8");
                    String addonUrl = new String(addon.getElementsByTagName("addon_url").item(0).getTextContent().getBytes(encoding), "UTF-8");
                    
                    String message = Colors.BOLD + addonName + Colors.NORMAL + ", par " + addonAuthor + " : " + addonSummary;
                    
                    // On tronque le résultat s'il est trop grand
                    if (message.length() > 93)
                    {
                        message = message.substring(0, 90);
                        message = message.substring(0, message.lastIndexOf(" ")) + "...";
                    }
                    
                    message += " ( " + EsFrBot.shrinkUrl(addonUrl) + " ) ";
                    
                    bot.safeMessage(recipient, message);
                    
                    i++;
                }
                while (i < nbrResults);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            bot.safeMessage(recipient, "Erreur rencontrée durant la recherche (site indisponible ou mot-clé incorrect).");
        }
    }
}
