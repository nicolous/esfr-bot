/*
 * Copyright 2008-2011 Nicolas Maingot
 * 
 * This file is part of EsFr-Bot.
 *
 * EsFr-Bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * EsFr-Bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EsFr-Bot.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.eventscripts.commands;

import java.util.Scanner;


/**
 * Une commande IRC
 */
public abstract class Command
{
    /** Le nom de la commande */
    protected String name;
    
    /** Description de la commande */
    protected String description;
    
    /** Prépare une nouvelle commande
     * 
     * @param name Le nom de la commande
     * @param description La description de la commande
     */
    public Command(String theName, String theDescription)
    {
        name = theName;
        description = theDescription;
    }
    
    /** Retourne le nom de la commande */
    public String getName()
    {
        return name;
    }
    
    /** Retourne la description de la commande */
    public String getDescription()
    {
        return description;
    }
    
    /** Code de la commande
     * 
     * @param channel Le channel où la commande a été utilisée
     * @param sender L'utilisateur de la commande
     * @param commandLine Les arguments de la commande
     */
    public abstract void execute(String channel, String sender, Scanner commandLine);
}
