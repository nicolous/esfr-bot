/*
 * Copyright 2008-2011 Nicolas Maingot
 * 
 * This file is part of EsFr-Bot.
 *
 * EsFr-Bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * EsFr-Bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EsFr-Bot.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.eventscripts.pircbot;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/** 
 * Log les entrées/sorties du bot dans des fichiers différents selon la date
 */
public class BotLogger
{
    /** Format de la date dans les logs */
    final static String DATE_FORMAT = "d_MM_yyyy";
    
    /** Dossier de sortie des logs */
    private String directory;

    /** Date du fichier en cours d'écriture */
    private String fileDate;
    
    /** Buffer d'écriture  */
    private BufferedWriter writer;
    
    /** Prépare l'enregistrement des logs
     * 
     * @param folder Dossier d'enregistrement des logs
     */
    public BotLogger(String folder)
    {
        directory = folder;
        openLog();
    }
    
    /** Efface les logs vieux d'un mois */
    private void purgeLogs()
    {
          Date today = new Date();
        SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT);
        
        File logsFolder = new File(directory);
        if (logsFolder.exists())
        {
            for (File logFile : logsFolder.listFiles())
            {
                try
                {
                    Date oldDate = formater.parse(logFile.getName());
                    if ((today.getTime() - oldDate.getTime()) > 1000L * 60L * 60L * 24L * 30L) // 30 jours
                    {
                        if (! logFile.delete())
                            System.err.println("Impossible de supprimer le fichier " + logFile.getName());
                    }
                }
                catch (ParseException e)
                {
                    e.printStackTrace();
                }
            }
        }
        else
        {
            if (! logsFolder.mkdir())
                System.err.println("Impossible de créer le dossier logs");
        }        
    }
    
    /** Retourne le nom théorique du fichier de log courant */
    public File getLogFile()
    {
        Date today = new Date();
        SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT);
        fileDate = formater.format(today);
        return new File(directory + "/" + fileDate + ".txt");
    }
    
    /** Crée/Ouvre un nouveau fichier de logs */
    private void openLog()
    {
        purgeLogs();
        
        Date today = new Date();
        SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT);
        
        File logsFolder = new File(directory);
        if (logsFolder.exists())
        {
            if (logsFolder.canWrite())
            {
                try
                {
                    fileDate = formater.format(today);
                    File newLog = new File(directory + "/" + fileDate + ".txt");
                    if (! newLog.exists())
                        newLog.createNewFile();
                    
                    if (newLog.canWrite())
                    {
                        writer = new BufferedWriter(new FileWriter(newLog, true));
                        log("");
                        log("********** Début des logs **********");
                        log("");
                    }
                    else
                        System.err.println("Le fichier " + newLog.getName() + " n'a pas les droits en écriture");
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            else
                System.err.println("Le dossier logs n'a pas les droits en écriture");
        }        
    }
    
    /** Ferme le fichier de log */
    public void closeLog()
    {
        if (writer != null)
        {
            try
            {    
                writer.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }    
    
    /** Log du texte
     * 
     * @param line La ligne à écrire
     */
    public void log(String line)
    {
        // Changement de fichier si nécessaire (selon la date)
        Date now = new Date();
        SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT);
        if (! fileDate.equals(formater.format(now)))
        {
            closeLog();
            openLog();
        }
        
        // Ecriture dans le fichier
        if (writer != null)
        {
            formater = new SimpleDateFormat("[d/MM/yyyy HH:mm:ss]");
            String text = formater.format(now) + " " + line;
            
            System.out.println(text);
            try
            {
                writer.append(text);
                writer.newLine();
                writer.flush();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
