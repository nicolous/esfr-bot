/*
 * Copyright 2008-2011 Nicolas Maingot
 * 
 * This file is part of EsFr-Bot.
 *
 * EsFr-Bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * EsFr-Bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EsFr-Bot.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.eventscripts.pircbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/** 
 * Interface de saisie de commandes au clavier 
 */
public class BotConsole extends Thread
{
    /** Buffer pour la saisie au clavier */
    private BufferedReader reader;
    
    /** Référence sur le bot IRC */
    private EsFrBot bot;
    
    /**  
     * @param botIrc Référence sur le bot auquel appartient l'annonceur
     */
    public BotConsole(EsFrBot botIrc)
    {
        bot = botIrc;
        reader = new BufferedReader(new InputStreamReader(System.in));
    }
    
    @Override
    public void run()
    {
        while (true)
        {
            try
            {
                String command = reader.readLine();
                if (command != null)
                    bot.sendRawLineViaQueue(command);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
