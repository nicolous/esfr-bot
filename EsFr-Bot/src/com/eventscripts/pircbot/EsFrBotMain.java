/*
 * Copyright 2008-2011 Nicolas Maingot
 * 
 * This file is part of EsFr-Bot.
 *
 * EsFr-Bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * EsFr-Bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EsFr-Bot.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.eventscripts.pircbot;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;

import org.jibble.pircbot.Colors;
import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.NickAlreadyInUseException;
import org.jibble.pircbot.User;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.eventscripts.commands.Command;
import com.eventscripts.discussion.DefaultConversation;
import com.eventscripts.discussion.TestDialog;
import com.eventscripts.esam.AddonSearchTask;
import com.eventscripts.esam.AddonSearchTask.SearchTypeCode;
import com.eventscripts.pastebin.Pastebin;
import com.eventscripts.pastebin.PastebinBuilder;
import com.eventscripts.rss.EsamRss;
import com.eventscripts.rss.RssChannelWatcherException;
import com.eventscripts.rss.RssReader;
import com.eventscripts.rss.SteamGroupRss;
import com.eventscripts.rss.WikiRss;


public class EsFrBotMain 
{
    public static final String VERSION = "1.4b";
    public static Random rand = new Random();
    
    public static void spawnWikiSpirit(EsFrBot bot, String channel)
    {
        if (rand.nextInt(101) <= 10)
        {
            User spirit = null;
            for (User user : bot.getUsers(channel))
                if (user.getNick().equals("WikiSpirit"))
                {
                    spirit = user;
                    break;
                }
                
            if (spirit == null)
            {
                bot.safeMessage(channel, "WIKI ! WIKI ! WIKIIIIIIIIIIIIIIIIII !!!");
                bot.sendRawLineViaQueue("PRIVMSG WikiSpirit :!cmd join " + channel);
            }
            else
                bot.sendRawLineViaQueue("PRIVMSG WikiSpirit :!cmd part " + channel);
        }
    }
    
    private static void addOpCommands(final EsFrBot bot)
    {
        bot.addOpCommand("execute", new Command("execute", "!" + bot.getName() + " execute ligne de commande (Exécute une commande IRC)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                    bot.sendRawLine(commandLine.nextLine());
                else
                    bot.safeNotice(sender, "Merci de spécifier une ligne de commande.");
            }
        });
        
        bot.addOpCommand("action", new Command("action", "!" + bot.getName() + " action Le message (Equivalent de /me Le message)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {    
                if (commandLine.hasNext())
                {
                    String targetChannel = commandLine.next();
                    if (targetChannel.startsWith("#"))
                    {    
                        if (commandLine.hasNext())
                        {
                            commandLine.skip(" ");
                            bot.sendCTCPCommand(targetChannel, "ACTION " + commandLine.nextLine());
                        }
                        else
                            bot.safeNotice(sender, "Merci de spécifier un message.");
                    }
                    else
                        bot.safeNotice(sender, "Le nom du channel cible est invalide.");
                }
                else
                    bot.safeNotice(sender, "Merci de spécifier un channel et un message.");
            }
        });
        
        bot.addOpCommand("disconnect", new Command("disconnect", "!" + bot.getName() + " disconnect (Déconnecte le bot du serveur)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                bot.quit("A plus tardivement !");
            }
        });
        
        bot.addOpCommand("part", new Command("part", "!" + bot.getName() + " part #channel (Déconnecte le bot d'un channel)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    String nomChannel = commandLine.next(); 
                    if (nomChannel.startsWith("#"))
                    {
                        bot.partChannel(nomChannel, "A plus tardivement !");
                        bot.removeDefaultChannel(nomChannel);
                    }
                    else
                        bot.safeNotice(sender, "Nom de channel invalide.");
                }
                else
                    bot.safeNotice(sender, "Merci de spécifier un nom de channel.");
            }
        });
        
        bot.addOpCommand("join", new Command("join", "!" + bot.getName() + " join #channel (Connect le bot à un channel)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    String channelName = commandLine.next();
                    if (channelName.startsWith("#"))
                    {
                        String pass = null;
                        if (commandLine.hasNext()) // Mot de passe ?
                        {
                            pass = commandLine.next();
                            bot.joinChannel(channelName, pass);
                        }
                        else
                            bot.joinChannel(channelName);
                        
                        bot.addDefaultChannel(channelName, pass);
                    }
                    else
                        bot.safeNotice(sender, "Nom de channel invalide.");
                }
                else
                    bot.safeNotice(sender, "Merci de spécifier un nom de channel.");
            }
        });
        
        bot.addOpCommand("say", new Command("say", "!" + bot.getName() + " say Le message (Fait parler le bot)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    String targetChannel = commandLine.next();
                    if (targetChannel.startsWith("#"))
                    {    
                        if (commandLine.hasNext())
                        {
                            commandLine.skip(" ");
                            bot.safeMessage(targetChannel, commandLine.nextLine());
                        }
                        else
                            bot.safeNotice(sender, "Merci de spécifier un message.");
                    }
                    else
                        bot.safeNotice(sender, "Le nom du channel cible est invalide.");
                }
                else
                    bot.safeNotice(sender, "Merci de spécifier un channel et un message.");
            }
        });
        
        bot.addOpCommand("conversation", new Command("conversation", "!" + bot.getName() + " conversation on/off)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    String mode = commandLine.next();
                    if (mode.equals("on"))
                    {
                        bot.setConversationHandler(new TestDialog(bot));
                        bot.safeNotice(sender, "conversation on.");
                    }
                    else if (mode.equals("off"))
                    {
                        bot.setConversationHandler(new DefaultConversation(bot));
                        bot.safeNotice(sender, "conversation off.");
                    }
                    else
                        bot.safeNotice(sender, "Les modes possibles sont on et off.");
                }
                else
                    bot.safeNotice(sender, "Merci de spécifier un mode.");
            }
        });        
    }
    
    private static void addPublicCommands(final EsFrBot bot)
    {
        bot.addPublicCommand("pygo", new Command("pygo","!pygo http://code.eventscripts.com/id [nom-du-script]")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    commandLine.skip(" ");
                    String url = commandLine.next();
                    //if (url.startsWith("http://"))
                    if (Pattern.matches("http://code\\.eventscripts\\.com/[0-9]+", url))
                    {
                        String scriptname = null;
                        if (commandLine.hasNext())
                            scriptname = commandLine.next();
                        else
                        {
                            bot.safeMessage(channel, "Attention : aucun nom de script n'a été spécifier. " +
                                    "La traduction des es_doblock ne pourra pas être optimale.");
                            scriptname = "temp";
                        }
//                        int iResource = url.lastIndexOf('/');
//                        if (iResource != -1)
//                        {
                        //PastebinBuilder builder = new PastebinBuilder(bot, url.substring(0, iResource));
                        PastebinBuilder builder = new PastebinBuilder(bot, "http://code.eventscripts.com");
                        Pastebin essPaste = null;
                        try
                        {
                            essPaste = builder.getPastebin(url);
                        }
                        catch (ParserConfigurationException e)
                        {
                            e.printStackTrace();
                            bot.safeMessage(channel, "Erreur interne.");
                        }
                        catch (SAXException e)
                        {
                            e.printStackTrace();
                            bot.safeMessage(channel, "Erreur durant la récupération du code. (Site-Pastebin non supporté ?)");
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                            bot.safeMessage(channel, "Erreur durant la connexion au site " + builder.getWebsite());
                        }
                        
                        if (essPaste != null)
                        {
                            BufferedWriter tempOut = null;
                            File essFile = null;
                            File pyFile = null;
                            try
                            {
                                essFile = new File("es_" + scriptname + ".txt");
                                if (essFile.exists())
                                    essFile.delete();
                                pyFile = new File("out/" + essFile.getName().substring(3).replace(".txt", ".py"));
                                if (pyFile.exists())
                                    pyFile.delete();
                                
                                essFile.createNewFile();
                                tempOut = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(essFile)));
                                tempOut.write(essPaste.getCode());
                                tempOut.flush();
                            }
                            catch (IOException e)
                            {
                                e.printStackTrace();
                                bot.safeMessage(channel, "Erreur interne.");
                            }

                            if (tempOut != null)
                            {
                                ProcessBuilder launcher = new ProcessBuilder("python", "pytranslator/translator.py", essFile.getAbsolutePath());
                                
                                // pour que sys.stdout.encoding soit différent de None
                                launcher.environment().put("PYTHONIOENCODING","utf-8");
                                // ne fonctionne qu'avec Python 2.6+
                                
                                launcher.redirectErrorStream(true);
                                Process pytranslator = null;
                                try
                                {
                                    pytranslator = launcher.start();
                                }
                                catch (IOException e)
                                {
                                    bot.safeMessage(channel, "Erreur interne.");
                                    e.printStackTrace();
                                }

                                if (pytranslator != null)
                                {
                                    try
                                    {
                                        pytranslator.waitFor();
                                    }
                                    catch (InterruptedException e)
                                    {
                                        e.printStackTrace();
                                    }
                                    try
                                    {
                                        tempOut.close();
                                    }
                                    catch (IOException e)
                                    {
                                        e.printStackTrace();
                                    }
                                    
                                    BufferedReader translatorOutput = new BufferedReader(new InputStreamReader(pytranslator.getInputStream()));                                    
                                    BufferedReader pyInput = null;                                    
                                    StringBuilder output = new StringBuilder();
                                    StringBuilder newCode = new StringBuilder();
                                    StringBuilder emlibCode = new StringBuilder(); 
                                    String line = null;
                                    try
                                    {
                                        line = translatorOutput.readLine();
                                        while (line != null)
                                        {
                                            output.append(line).append('\n');
                                            System.out.println(line);                                            
                                            line = translatorOutput.readLine();
                                        }
                                        if (pyFile.exists())
                                        {
                                            pyInput = new BufferedReader(new InputStreamReader(new FileInputStream(pyFile)));
                                            line = pyInput.readLine();
                                            while (line != null)
                                            {
                                                newCode.append(line).append("\n");
                                                line = pyInput.readLine();
                                            }
                                        }
                                    }
                                    catch (IOException e)
                                    {
                                        e.printStackTrace();
                                        bot.safeMessage(channel, "Erreur interne.");
                                    }
                                    finally
                                    {
                                        try
                                        {
                                            translatorOutput.close();
                                        }
                                        catch (IOException e)
                                        {
                                            e.printStackTrace();
                                        }
                                        
                                        if (pyInput != null)
                                            try
                                            {
                                                pyInput.close();
                                            }
                                            catch (IOException e)
                                            {
                                                e.printStackTrace();
                                            }
                                    }
                                    File emlibFile = new File("out/emlib.py");
                                    BufferedReader emlibInput = null;
                                    try
                                    {
                                        Pastebin outputPaste = null; 
                                        Pastebin pyPaste = null;
                                        Pastebin emlib = null;
                                            
                                        outputPaste = builder.newCode(essPaste, "text", output.toString());
                                        if (outputPaste != null)
                                        {
                                            if (newCode.length() > 0)
                                            {
                                                pyPaste = builder.newCode(essPaste, "python", newCode.toString());
                                                if (pyPaste != null)
                                                    if (pyFile.exists())
                                                    {
                                                        emlibInput = new BufferedReader(new InputStreamReader(new FileInputStream(emlibFile)));
                                                        line = emlibInput.readLine();
                                                        while (line != null)
                                                        {
                                                            emlibCode.append(line)
                                                                     .append('\n');
                                                            line = emlibInput.readLine();
                                                        }
                                                        
                                                        emlib = builder.newPastebin("python", emlibCode.toString());
                                                    }
                                            }
                                            
                                            bot.safeMessage(channel, sender);
                                            bot.safeMessage(channel, "Sortie : " + outputPaste.getUrl());
                                            
                                            if (pyPaste != null)
                                            {
                                                bot.safeMessage(channel, scriptname + ".py : " + pyPaste.getUrl());
                                                
                                                if (emlib != null)
                                                    bot.safeMessage(channel, "emlib.py : " + emlib.getUrl());
                                                bot.safeMessage(channel, "^^");                                                        
                                            }
                                            else
                                                bot.safeMessage(channel, "Le code Python n'a pas pu être généré.");
                                        }
                                        else
                                            bot.safeMessage(channel, "J'ai rencontré une erreur vraiment très étrange :o");
                                    }
                                    catch (IOException e)
                                    {
                                        e.printStackTrace();
                                        bot.safeMessage(channel, "Erreur durant la connexion au site.");
                                    }
                                    finally
                                    {
                                        if (emlibInput != null)
                                            try
                                            {
                                                emlibInput.close();
                                            }
                                            catch (IOException e)
                                            {
                                                e.printStackTrace();
                                            }
                                    }
                                }
                            }
                        }
                        else
                            bot.safeMessage(channel, "J'ai perdu mes marques... Le site aurait-il changé ?");
//                        }
                    }
                    else
                        //bot.safeMessage(channel, "Seuls les adresses commençant par http:// sont supportées.");
                        bot.safeMessage(channel, "Merci de poster le code sur site http://code.eventscripts.com");
                }
                else
                    bot.safeMessage(channel, "Merci de spécifier un lien vers http://code.eventscripts.com et, si possible, un nom de script.");
            }
            
        });
        
//        bot.addPublicCommand("logs", new Command("logs", "!logs (Permet de lire les logs de la journée)")
//        {
//            @Override
//            public void execute(String channel, String sender,
//                    Scanner commandLine)
//            {
//                File logFile = bot.getLogger().getLogFile();
//                if (logFile.exists())
//                {
//                    StringBuilder text = new StringBuilder();
//                    BufferedReader reader = null;
//                    try
//                    {
//                        reader = new BufferedReader(new InputStreamReader(new FileInputStream(logFile)));
//                        String line = reader.readLine();
//                        while (line != null)
//                        {
//                            text.append(line).append('\n');
//                            line = reader.readLine();
//                        }
//                    }
//                    catch (IOException e)
//                    {
//                        e.printStackTrace();
//                        bot.safeMessage(channel, "Erreur durant la récupération des logs.");
//                    }
//                    finally
//                    {
//                        if (reader != null)
//                        {
//                            try
//                            {
//                                reader.close();
//                            }
//                            catch (IOException e)
//                            {
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//                    
//                    System.out.println(text.toString());
//                    PastebinBuilder builder = new PastebinBuilder(bot, "http://code.eventscripts.com");
//                    try
//                    {
//                        Pastebin logsPastebin = builder.newPastebin("text", text.toString());
//                        if (logsPastebin != null)
//                        {
//                            bot.safeMessage(channel, logsPastebin.getUrl());
//                        }
//                    }
//                    catch(IOException e)
//                    {
//                        e.printStackTrace();
//                        bot.safeMessage(channel, "Erreur durant la mise en ligne des logs sur " + builder.getWebsite());
//                    }
//                }
//                else
//                    bot.safeMessage(channel, "Aucun log disponible.");
//            }
//        });
        
        bot.addPublicCommand("wiki", new Command("wiki", "!wiki commande (Permet de rechercher une commande sur le wiki ESS)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    commandLine.skip(" ");
                    
                    //GoogleSearchTask search = new GoogleSearchTask(bot, commandLine.nextLine() + "%20site:www.eventscripts.com/pages%20-User:", false, channel, 2);
                    //search.start();
                    String url = "http://www.google.fr/search" +
                        "?hl=fr" +
                        "&q=" +  commandLine.nextLine().replaceAll("\\s+", "+") + "%20site:www.eventscripts.com/pages";
                    bot.safeMessage(channel, url);
                    
                    //spawnWikiSpirit(bot, channel);
                }
                else
                    bot.safeMessage(channel, "Merci de spécifier un nom de commande.");
            }
        });

        bot.addPublicCommand("wikib", new Command("wikib", "!wikib page (Construit le nom d'une page du wiki ESS)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    commandLine.skip(" ");
                    bot.safeMessage(channel, "http://www.eventscripts.com/pages/" + commandLine.nextLine());
                }
                else
                    bot.safeMessage(channel, "Merci de spécifier un nom de page.");
            }
        });        
        
        bot.addPublicCommand("wikifr", new Command("wikifr", "!wikifr commande (Permet de rechercher une commande sur la partie française du wiki ESS)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    commandLine.skip(" ");
                    
                    //GoogleSearchTask search = new GoogleSearchTask(bot, commandLine.nextLine() + "%20site:www.eventscripts.com/pages%20-User:", true, channel, 2);
                    //search.start();
                    String url = "http://www.google.fr/search" +
                        "?hl=fr" +
                        "&tbs=lr:lang_1fr" +
                        "&lr=lang_fr" +
                        "&q=" +  commandLine.nextLine().replaceAll("\\s+", "+") + "%20site:www.eventscripts.com/pages%20";
                    bot.safeMessage(channel, url);
                    
                    //spawnWikiSpirit(bot, channel);
                }
                else
                    bot.safeMessage(channel, "Merci de spécifier un nom de commande.");
            }
        });
        
        bot.addPublicCommand("pwiki", new Command("pwiki", "!pwiki commande (Permet de rechercher une commande sur le wiki ESP)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    commandLine.skip(" ");
                    
                    //GoogleSearchTask search = new GoogleSearchTask(bot, commandLine.nextLine() + "%20site:python.eventscripts.com/pages%20-User:", false, channel, 2);
                    //search.start();
                    String url = "http://www.google.fr/search" +
                        "?hl=fr" +
                        "&q=" +  commandLine.nextLine().replaceAll("\\s+", "+") + "%20site:python.eventscripts.com/pages%20";
                    bot.safeMessage(channel, url);
                    
                    //spawnWikiSpirit(bot, channel);
                }
                else
                    bot.safeMessage(channel, "Merci de spécifier un nom de commande.");
            }
        });    
        
        bot.addPublicCommand("pwikib", new Command("pwikib", "!pwikib page (Construit le nom d'une page du wiki ESP)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    commandLine.skip(" ");
                    bot.safeMessage(channel, "http://python.eventscripts.com/pages/" + commandLine.nextLine());
                }
                else
                    bot.safeMessage(channel, "Merci de spécifier un nom de page.");
            }
        });                
        
        bot.addPublicCommand("pwikifr", new Command("pwikifr", "!pwikifr commande (Permet de rechercher une commande sur la partie française du wiki ESP)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    commandLine.skip(" ");
                    //GoogleSearchTask search = new GoogleSearchTask(bot, commandLine.nextLine() + "%20site:python.eventscripts.com/pages%20-User:", true, channel, 2);
                    //search.start();
                    String url = "http://www.google.fr/search" +
                        "?hl=fr" +
                        "&tbs=lr:lang_1fr" +
                        "&lr=lang_fr" +
                        "&q=" +  commandLine.nextLine().replaceAll("\\s+", "+") + "%20site:python.eventscripts.com/pages";
                    bot.safeMessage(channel, url);
                    
                    //spawnWikiSpirit(bot, channel);
                }
                else
                    bot.safeMessage(channel, "Merci de spécifier un nom de commande.");
            }
        });        
        
        bot.addPublicCommand("wikipedia", new Command("wikipedia", "!wikipedia mots-clés (Permet de rechercher un article sur wikipédia)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    commandLine.skip(" ");
                    
                    /*String argument = commandLine.nextLine().replaceAll("\\s", "_");
                    bot.safeMessage(channel, "http://fr.wikipedia.org/wiki/?search=" + argument);*/
                    /*GoogleSearchTask search = new GoogleSearchTask(bot, commandLine.nextLine() + "%20site:fr.wikipedia.org/wiki", false, channel, 2);
                    search.start();*/
                    String url = "http://www.google.fr/search" + 
                    "?hl=fr" +
                    "&tbs=lr:lang_1fr" +
                    "&lr=lang_fr" +
                    "&q=" +  commandLine.nextLine().replaceAll("\\s+", "+") + "%20site:fr.wikipedia.org/wiki";
                    bot.safeMessage(channel, url);                    
                }
                else
                    bot.safeMessage(channel, "Merci de spécifier un nom de commande.");
            }
        });    
        
        bot.addPublicCommand("google", new Command("google", "!google mots-clés (Permet de faire une recherche sur google)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    commandLine.skip(" ");
                    String arguments = commandLine.nextLine().replaceAll("\\s", "+");
                    bot.safeMessage(channel, "http://www.google.fr/search?hl=fr&q=" + arguments);
                }
                else
                    bot.safeMessage(channel, "Merci de spécifier des mots-clés.");
            }
        });    
        
        bot.addPublicCommand("!google", new Command("!google", "!!google mots-clés (Permet de faire une recherche sur google)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    commandLine.skip(" ");
                    String arguments = commandLine.nextLine().replaceAll("\\s", "+");
                    bot.safeMessage(channel, "http://letmegooglethatforyou.com/?q=" + arguments);
                }
                else
                    bot.safeMessage(channel, "Merci de spécifier des mots-clés.");
            }
        });    
        
        bot.addPublicCommand("script", new Command("script", "!script script (Permet de rechercher un script sur ESAM)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    String searchString = commandLine.next();
                    if (searchString.length() > 2)
                    {
                        AddonSearchTask search = new AddonSearchTask(bot, searchString, SearchTypeCode.ADDON, channel);
                        search.start();
                    }
                    else
                        bot.safeMessage(channel, "Le mot clé doit être composé d'au moins 3 caractères. (C'est crétin, je sais.)");
                }
                else
                    bot.safeMessage(channel, "Merci de spécifier un nom de script.");
            }
        });    
        
        bot.addPublicCommand("pastebin", new Command("pastebin", "!pastebin commande (Affiche des liens vers des sites de présentation de code)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                bot.safeMessage(channel, "- Pour du code ESS ou ESP : http://code.eventscripts.com");
                bot.safeMessage(channel, "- Pour un autre langage : http://www.pastebin.com");
            }
        });
        
        bot.addPublicCommand("addons", new Command("addons", "!addons (N'existe plus, utilisez !auteur)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                bot.safeMessage(channel, "Cette commande a été remplacée par la commande !auteur");
            }
        });
        
        bot.addPublicCommand("auteur", new Command("auteur", "!auteur auteur (Permet de rechercher les scripts d'un auteur)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    String author = commandLine.next();
        
                    if (author.length() > 2)
                    {
                        AddonSearchTask search = new AddonSearchTask(bot, author, SearchTypeCode.USER, channel);
                        search.start();
                    }
                    else
                        bot.safeMessage(channel, "Le mot clé doit être composé d'au moins 3 caractères. (C'est crétin, je sais.)");
                }
                else
                    bot.safeMessage(channel, "Merci de spécifier un nom d'auteur.");
            }
        });    
        
        bot.addPublicCommand("esam", new Command("esam", "!esam mots-clés (Permet de faire une recherche ciblée sur le site http://addons.eventscripts.com)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    commandLine.skip(" ");
                    /*GoogleSearchTask search = new GoogleSearchTask(bot, commandLine.nextLine() + "%20site:addons.eventscripts.com", false, channel, 5);
                    search.start();*/
                    String searchString = commandLine.next();
                    if (searchString.length() > 2)
                        bot.safeMessage(channel, "http://addons.eventscripts.com/addons/search/" + searchString);
                    else
                        bot.safeMessage(channel, "Le mot clé doit être composé d'au moins 3 caractères. (C'est crétin, je sais.)");
                }
                else
                    bot.safeMessage(channel, "Merci de spécifier un ou plusieurs mots-clés.");
            }
        });
        
        bot.addPublicCommand("code", new Command("code", "!code mots-clés (Permet de chercher des exemples de code sur http://code.eventscripts.com)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    commandLine.skip(" ");
                    /*GoogleSearchTask search = new GoogleSearchTask(bot, commandLine.nextLine() + "%20site:code.eventscripts.com", false, channel, 5);
                    search.start();*/
                    String url = "http://www.google.fr/search" + 
                    "?q=" +
                    commandLine.nextLine().replaceAll("\\s+", "+") + "%20site:code.eventscripts.com";
                    bot.safeMessage(channel, url);
                }
                else
                    bot.safeMessage(channel, "Merci de spécifier un ou plusieurs mots-clés.");
            }
        });
        
        bot.addPublicCommand("news", new Command("news", "!news (Affiche la dernière news)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                RssReader reader = new RssReader("http://steamcommunity.com/groups/eventscripts-fr/rss");
                try
                {
                    Document document = reader.openChannel();

                    NodeList items = document.getElementsByTagName("item");

                    int nbItems = items.getLength();
                    if (nbItems > 3)
                        nbItems = 3;
                    
                    //bot.safeMessage(channel, "Dernières news :");
                    while (nbItems > 0)
                    {
                        nbItems--;
                        Element item = (Element)items.item(nbItems);
                        
                        boolean valid = true;
    
                        String title = "";
                        NodeList titleNode = item.getElementsByTagName("title");
                        if (titleNode.getLength() > 0)
                            title = titleNode.item(0).getTextContent();
                        else
                            valid = false;
    
                        String link = "";
                        NodeList linkNode = item.getElementsByTagName("link");
                        if (linkNode.getLength() > 0)
                            link = linkNode.item(0).getTextContent();
                        else
                            valid = false;
    
                        /*String description = "";
                        NodeList descriptionNode = item.getElementsByTagName("description");
                        if (descriptionNode.getLength() > 0)
                            description = descriptionNode.item(0).getTextContent();
                        else
                            valid = false;*/
    
                        if (valid)
                            bot.safeMessage(channel, title + " : " + EsFrBot.shrinkUrl(link));
                    }
                    bot.safeMessage(channel,
                            Colors.BOLD + "Retrouvez toutes les news sur " + Colors.NORMAL + " http://steamcommunity.com/groups/eventscripts-fr/announcements");
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });        
        
        bot.addPublicCommand("astuce", new Command("astuce", "!astuce id (Affiche des astuces)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                if (commandLine.hasNext())
                {
                    commandLine.skip(" ");
                    String id = commandLine.nextLine();
                    
                    if (id.equals("pseudo"))
                    {
                        bot.safeMessage(channel, "Connaissez-vous l'astuce de la tabulation pour auto-compléter les pseudos sur IRC ?");
                        bot.safeMessage(channel, "Par exemple, en tapant \"Le<TAB>\", vous obtenez \"Lena\" si l'un des utilisateurs du channel se nomme Lena");
                    }
                    else if (id.equals("question"))
                        bot.safeMessage(channel, "Ne demandez pas si vous pouvez demander quelque chose, demandez ce que vous voulez !");
                }
                else
                    bot.safeMessage(channel, "Merci de spécifier l'identifiant d'une astuce.");
            }
        });        
        
        bot.addPublicCommand("version", new Command("version", "!version (Affiche la version actuelle de Lena)")
        {
            @Override
            public void execute(String channel, String sender, Scanner commandLine)
            {
                bot.safeMessage(channel, VERSION);
            }
        });
    }
    
    public static void main(String [] args) throws Exception
    {           
        if (args.length >= 2)
        {
            // Construction du bot
            String authLine = args[1];
            EsFrBot bot = new EsFrBot(authLine, "+x", "Lena", "Version " + VERSION);
            
            // Ajout des commandes publiques et privées 
            addOpCommands(bot);
            addPublicCommands(bot);    
            
            try
            {
                // Lancement de la surveillance des mises à jour des scripts sur ESAM
                /*AddonUpdateManager updateManager = new AddonUpdateManager(bot);
                updateManager.start();*/
                SimpleDateFormat esamDateFormat = new SimpleDateFormat("yyyy-MM-d HH:mm:ss", Locale.US);
                EsamRss esam = 
                    new EsamRss(bot,
                                1000 * 60 * 15, // 15 min
                                esamDateFormat,
                                "http://addons.eventscripts.com/addons/rss/updates");
                new Thread(esam).start();
    
                // Lancement de la surveillance des mises à jour des wikis
                SimpleDateFormat wikiDateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z", Locale.US);
                WikiRss rssWikiEss =
                    new WikiRss(bot,
                                1000 * 60 * 15, // 15 min
                                wikiDateFormat,
                                "http://www.eventscripts.com/index.php?title=Special:Recentchanges&limit=10&days=1&feed=rss");
                //new Thread(rssWikiEss).start();
                
                WikiRss rssWikiEsp =
                    new WikiRss(bot,
                                1000 * 60 * 15, // 15 min
                                wikiDateFormat,
                                "http://python.eventscripts.com/index.php?title=Special:Recentchanges&limit=10&days=1&feed=rss");    
                //new Thread(rssWikiEsp).start();
                
                // Lancement de la surveillance des news du groupe steam
                SimpleDateFormat esFrenchDateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.US);
                SteamGroupRss rssSteam =
                    new SteamGroupRss(  bot,
	                                    "#eventscripts-fr",
	                                    1000 * 60 * 15, // 15 min
	                                    esFrenchDateFormat,
	                                    "http://steamcommunity.com/groups/eventscripts-fr/rss");
                new Thread(rssSteam).start();
            }
            catch(RssChannelWatcherException e)
            {
                e.printStackTrace();
            }
            
            // Channels par défaut :
            for (int i = 2; i < args.length; i++)
                bot.addDefaultChannel(args[i], null);
            bot.addDefaultChannel("#esfr-bot", null);
            //bot.addDefaultChannel("#esfr-bot2", null);
            
            bot.setConversationHandler(new TestDialog(bot));
            
            try
            {
                if (args[0].indexOf(":") >= 0)
                {
                    String [] serverPlusPort = args[0].split(":");
                    bot.connect(serverPlusPort[0], Integer.parseInt(serverPlusPort[1]));
                }
                else
                    bot.connect(args[0]);
                
                // Lancement de la console du bot
                BotConsole console = new BotConsole(bot);
                console.start();
            }
            catch (NickAlreadyInUseException e)
            {
                System.out.println("Nickname already used");
            }
            catch (IOException e)
            {
                System.out.println("Unable to connect to " + args[0]);
            }
            catch (IrcException e)
            {
                System.out.println("Connection refused by " + args[0]);
            }
            catch (NumberFormatException e)
            {
                System.out.println("Invalid port number (" + e.getMessage() + ")");
            }
        }
        else
            System.out.println("Parameters : \"serveur[:port]\" [\"auth-command-line\"] channel1 [channel2 [channel3] ...]");
    }
}
