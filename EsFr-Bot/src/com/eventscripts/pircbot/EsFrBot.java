/*
 * Copyright 2008-2011 Nicolas Maingot
 * 
 * This file is part of EsFr-Bot.
 *
 * EsFr-Bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * EsFr-Bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EsFr-Bot.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.eventscripts.pircbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

import org.jibble.pircbot.PircBot;
import org.jibble.pircbot.User;

import com.eventscripts.commands.Command;
import com.eventscripts.discussion.DefaultConversation;


/** 
 * Le bot IRC
 */
public class EsFrBot extends PircBot 
{	
	/** Commande soumise au serveur pour l'authenfitication du bot */
	private String authCommand;
	
	/** Mode IRC du bot */
	private String botMode;
	
	/** Est-ce que le bot est autorisé à essayer de se reconnecter au serveur en cas de déconnexion ? */
    private boolean allowReconnect;
    
    /** Commandes publiques */
    private HashMap<String, Command> publicCommands;
    
    /** Commandes admins */
    private HashMap<String, Command> opCommands;
    
    /** Gestion des messages reçus */
    private DefaultConversation conversation;
    
    /** Gestion des logs */
    private BotLogger logger;
    
    /* Lamer bot */
    //private LamerBot lamerbot;
    
    /** Map des {channel => pass } où le bot doit rejoindre lors qu'il s'est connecté */
    private HashMap<String, String> defaultChannels;

    /**  
     * @param authLine La ligne de commande permettant au bot d'être authentifié sur le serveur (peut être null)
     */
    public EsFrBot(String authLine, String mode, String name, String version) 
    {
        authCommand = authLine;
        botMode = mode;
        
        setName(name);
        setLogin(name);
        setVersion(version);
        setAutoNickChange(true); // Si le pseudo est déjà pris...
        setVerbose(true); // déboguage...
        
        try
        {
            setEncoding("UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
        
        allowReconnect = true;
        
        publicCommands = new HashMap<String, Command>();
        opCommands = new HashMap<String, Command>();
        conversation = new DefaultConversation(this);
        defaultChannels = new HashMap<String, String>();
        
        logger = new BotLogger("logs");
        //lamerbot = new LamerBot();
    }
    
    /** Retourne le logger courant */
    public BotLogger getLogger()
    {
        return logger;
    }
    
    @Override
    public void log(String line)
    {
        logger.log(line);
    }
    
    /*public LamerBot getLamerbot()
    {
        return lamerbot;
    }*/
    
    /** Ajoute une commande publique au bot
     * 
     * @param name Le nom de la commande à ajouter
     * @param command La commande à ajouter
     */
    public void addPublicCommand(String name, Command command)
    {
        publicCommands.put(name, command);
    }
    
    /** Ajoute une commande administrateur au bot
     *
     * @param name Le nom de la commande à ajouter
     * @param command La commande à ajouter
     */
    public void addOpCommand(String name, Command command)
    {
        opCommands.put(name, command);
    }
    
    /** Ajoute un channel par défaut
     * 
     * @param channel Le channel à rejoindre
     * @param pass Le mot de passe du channel
     */
    public void addDefaultChannel(String channel, String pass)
    {
        defaultChannels.put(channel, pass);
    }
    
    /** Détermine l'objet servant à analyser les messages de discussion adressés au bot
     * 
     * @param handler
     */
    public void setConversationHandler(DefaultConversation handler)
    {
        conversation = handler;
    }
    
    /** Supprime un channel par défaut
     * 
     * @param channel Le channel à supprimer
     */
    public void removeDefaultChannel(String channel)
    {
        defaultChannels.remove(channel);
    }

    /** Envoie un message sur tous les channels où le bot est connecté
     * 
     * @param message Le message à envoyer
     * @param channels Le channel où les résultats doivent être affichés
     */
    public void globalMessage(String [] channels, String message)
    {
        for (String channel : channels)
        {
            safeMessage(channel, message);
        }
    }
    
    /** Envoie un message à un channel ou un utilisateur en évitant le flood
     * 
     * @param recipient Le destinataire du message
     * @param message Le message
     */
    public void safeMessage(String target, String message)
    {
        setMessageDelay(1000 + 100 * getOutgoingQueueSize());
        sendMessage(target, message);
    }
 
    /** Envoie une notice à un channel ou un utilisateur en évitant le flood
     * 
     * @param recipient Le destinataire du message
     * @param message Le message
     */
    public void safeNotice(String target, String message)
    {
        setMessageDelay(1000 + 100 * getOutgoingQueueSize());
        sendNotice(target, message);
    }    
    
    /** Retrouve un utilisateur par rapport à son pseudo
     * 
     * @param name Le pseudo de l'utilisateur
     * @param channel Le channel où se trouve l'utilisateur et notre bot
     * @return L'objet java correspondant à l'utilisateur (peut être null !)
     */
    public User getUserByName(String name, String channel)
    {
        User found = null;
        User [] users = getUsers(channel);
        
        for (User user : users)
        {
            if (user.getNick().equals(name))
            {
                found = user;
                break;
            }
        }
        return found;
    }
    
    /** Exécute les commandes nécessitant d'être op
     * 
     * @param channel Le channel où les réponses publiques seront données (peut être null)
     * @param user L'utilisateur de la commande
     * @param commandLine Le buffer contenant la ligne de commande
     * @return <code>true</code> si une commande a été exécutée
     */
    public boolean parseOpCommand(String channel, User user, Scanner commandLine)
    {
        boolean matches = false;
        
        String sender = user.getNick();
        
        if (commandLine.hasNext())
        {
            String command = commandLine.next();
            
            if (command.equals("help"))
            {
                Set<String> commandsName = opCommands.keySet();
                for (String commandName : commandsName)
                {
                    Command jCommand = (Command)opCommands.get(commandName);
                    safeMessage(sender, jCommand.getDescription());
                }
            }
            else if (user.isOp())
            {
                if (opCommands.containsKey(command))
                {
                    opCommands.get(command).execute(channel, sender, commandLine);
                    matches = true;
                }
                else
                    safeNotice(sender, "Hein ?! Po compris. Essaye \"!" + getNick() + " help\"");
            }
            else
                safeNotice(sender, "Vos papiers s'il vous plait");
        }

        return matches;
    }

    /** Cherche une commande publique dans un message et l'excute
     * 
     * @param channel Le channel où le message a été tapé
     * @param sender L'auteur du message
     * @param command La pseudo-commande à identifier
     * @param commandLine Les arguments de la commande
     * @return <code>true</code> si une commande publique a été exécutée
     */
    private boolean parsePublicCommands(String channel, String sender, String command, Scanner commandLine)
    {
        boolean matches = false;
        
        if (command.equals("help") && (! commandLine.hasNext()))
        {
            Set<String> commandsName = publicCommands.keySet();
            for (String commandName : commandsName)
            {    
                Command jCommand = (Command)publicCommands.get(commandName);
                safeMessage(sender, jCommand.getDescription());
            }
        }
        else if (publicCommands.containsKey(command))
        {
            publicCommands.get(command).execute(channel, sender, commandLine);
            matches = true;
        }
        
        return matches;
    }
    
    @Override
    public void onMessage(String channel, String sender, String login, String hostname, String message) 
    {
        Scanner commandLine = new Scanner(message);
        String command = commandLine.next().toLowerCase();
    
        String nick = getNick().toLowerCase();
        
        // Recherche de l'utilisateur correspondant
        User user = getUserByName(sender, channel);
        
        // Est-ce que le message est une commande publique ?
        if (command.startsWith("!") && command.length() > 1)// && commandLine.hasNext())
        {
            String publicCommand = command.substring(1); // ignore le "!"
            
            // Est-ce que ce message est une commande adressée à notre bot ?
            if (publicCommand.equalsIgnoreCase(nick))
            {
                if (user != null)
                    parseOpCommand(channel, user, commandLine);
                else
                    safeNotice(sender, "On s'connait ?");
            }
            else
                parsePublicCommands(channel, sender, publicCommand, commandLine);
        }
        else
        {
            boolean op = false;
            if (user != null)
                op = user.isOp();
            conversation.parseText(sender, op, channel, message);
        }
    }
    
    @Override
    protected void onPrivateMessage(String sender, String login, String hostname, String message)
    {
        Scanner commandLine = new Scanner(message);
        
        if (commandLine.hasNext())
        {
            String command = commandLine.next();
            boolean isCommand = false; 
            
            // Recherche d'une commande public
            if (command.startsWith("!") && command.length() > 1)
                isCommand = parsePublicCommands(sender, sender, command.substring(1), commandLine);
            
            // Recherche d'une commande opérateur
            if (! isCommand)
            {    
                String channel = command;
            
                User user = getUserByName(sender, channel);
                if (user != null)
                    isCommand = parseOpCommand(channel, user, commandLine);
/*                else
                    safeNotice(sender, "Pour utiliser une commande opérateur, merci de préciser un channel sur lequel nous nous trouvons tous les deux.");*/
            }
        }
    }
    
    @Override
    protected void onJoin(String channel, String sender, String login, String hostname)
    {            
        if (sender.equals(getNick()))
            conversation.greetings(channel);
    }
    
    @Override
    protected void onPart(String channel, String sender, String login, String hostname)
    {
        if (sender.equals(getNick()))
            conversation.bye(channel);
    }
    
    @Override
    protected void onKick(String channel, String kickerNick, String kickerLogin, String kickerHostname, String recipientNick, String reason)
    {
        if (recipientNick.equalsIgnoreCase(getNick())) 
            joinChannel(channel);
    }
    
    @Override
    protected void onConnect()
    {
        // Authentification
        if (authCommand != null)
            sendRawLine(authCommand);
        
        // Tentons de cacher notre masque
        setMode(getNick(), botMode);
        
        // Attente de la réponse
        try
        {
            Thread.sleep(2000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        
        // Connexion aux channels par défaut
        //System.out.println(defaultChannels);
        Set<String> channels = defaultChannels.keySet(); 
        for (String channel : channels)
        {
            String pass = defaultChannels.get(channel);
            
            if (pass != null)
                joinChannel(channel, pass);
            else
                joinChannel(channel);
        }
        
        /*try
        {
            lamerbot.connect(getServer());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }*/
    }
    
    /** Déconnecte définitivement le bot
     * 
     * @param reason La raison de la déconnexion
     */
    public void quit(String reason)
    {
        // Fin du programme
        conversation.disconnect();
        quitServer(reason);
        allowReconnect = false;
        logger.closeLog();
        dispose();
        System.exit(0);
    }
    
    @Override
    protected void onDisconnect()
    {
        // Reconnexion si nécessaire
        while (allowReconnect && ! isConnected()) 
        {
            try
            {
                System.out.println("Prochaine tentative de reconnexion dans 5 minutes...");
                Thread.sleep(1000*60*5);
            
                System.out.println("Tentative de reconnexion...");
                reconnect();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    }
    
    /** Message retardé */
    private class DelayedMessage extends Thread
    {
        private long delay;
        private EsFrBot bot;
        private String target;
        private String message;
        
        public DelayedMessage(EsFrBot theBot, long theDelay, String theTarget, String theMessage)
        {
            bot = theBot;
            delay = theDelay;
            target = theTarget;            
            message = theMessage;
        }
        
        @Override
        public void run()
        {
            try
            {
                Thread.sleep(delay);
                
                bot.safeMessage(target, message);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }
    
    public void delayedMessage(long delay, String target, String message)
    {
        new DelayedMessage(this, delay, target, message).start();
    }
    
    /** Réduit une url
     * 
     * @param url L'url à réduire
     * @return Une version réduite de l'url
     * @throws IOException
     */
    public static String shrinkUrl(String url) throws IOException
    {
        URL ri = new URL("http://is.gd/api.php?longurl=" + url);
        URLConnection connection = ri.openConnection();
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);
        
        InputStream stream = connection.getInputStream();
        BufferedReader streamReader = new BufferedReader(new InputStreamReader(stream));
        return streamReader.readLine();
    }
}
