/*
 * Copyright 2008-2011 Nicolas Maingot
 * 
 * This file is part of EsFr-Bot.
 *
 * EsFr-Bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * EsFr-Bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EsFr-Bot.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.eventscripts.rss;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * Lecteur de flux RSS
 */
public class RssReader
{
    /** URL du flux RSS */
    protected String url;
    
    /**
     * @param rssUrl URL du flux RSS à lire
     */
    public RssReader(String rssUrl)
    {
        url = rssUrl;
    }
    
    /** Lit le flux RSS
     * 
     * @return Un Document XML
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     */
    public Document openChannel() throws IOException, ParserConfigurationException, SAXException
    {
        URL feedUrl = new URL(url);

        URLConnection connection = feedUrl.openConnection();
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);
        InputStream stream = connection.getInputStream();
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
       
        return builder.parse(stream);
    }
}
