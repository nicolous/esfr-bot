/*
 * Copyright 2008-2011 Nicolas Maingot
 * 
 * This file is part of EsFr-Bot.
 *
 * EsFr-Bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * EsFr-Bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EsFr-Bot.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.eventscripts.rss;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.eventscripts.pircbot.EsFrBot;

/** 
 * Lecture du flux RSS d'un mediawiki 
 */
public class WikiRss extends RssChannelWatcher
{
    /** Le bot IRC */
    private EsFrBot bot;
    
    /**
     * @param ircBot Le bot IRC
     * @see RssChannelWatcher
     * @throws RssChannelWatcherException
     */
    public WikiRss(EsFrBot ircBot, long watchInterval, SimpleDateFormat pubDateForm, String rssUrl)
        throws RssChannelWatcherException
    {
        super(watchInterval, pubDateForm, rssUrl);
        
        bot = ircBot;
    }

    @Override
    void read(ArrayList<Element> items)
    {
        int nbItems = items.size();
        if (nbItems > 0)
        {
            boolean valid = true;
            for (int i = 0; i< nbItems && valid; i++)
            {
                Element item = items.get(i);
                
                String title = "";
                NodeList titleNode = item.getElementsByTagName("title");
                if (titleNode.getLength() > 0)
                    title = titleNode.item(0).getTextContent();
                else
                    valid = false;
                if ((! title.startsWith("User:")) && (! title.startsWith("Special:")))
                {
                    String description = "";
                    NodeList descriptionNode = item.getElementsByTagName("description");
                    if (descriptionNode.getLength() > 0)
                        description = descriptionNode.item(0).getTextContent();
                    else
                        valid = false;
                    
                    String author = "";
                    NodeList authorNode = item.getElementsByTagName("dc:creator");
                    if (authorNode.getLength() > 0)
                        author = authorNode.item(0).getTextContent();
                    else
                        valid = false;
                    
                    String link = "";
                    NodeList linkNode = item.getElementsByTagName("link");
                    if (linkNode.getLength() > 0)
                        link = linkNode.item(0).getTextContent();
                    else
                        valid = false;                
                    
                    if (valid)
                    {
                        String comment = description.substring(description.indexOf("<p>") + 3, description.indexOf("</p>"));
                        
                        boolean newPage = description.startsWith("<p>" + comment + "</p>\n<p><b>New page");
                        
                        if (comment.length() > 0)
                            comment = " (" + comment + ")";
                        
                        String message = new String();
                        if (newPage)
                            message = " vient d'être créé par ";
                        else
                            message = " a été mis à jour par ";
    
                        bot.globalMessage(bot.getChannels(), "L'article " + link + message + author + comment);
                    }
                    else
                        System.err.println(url + " : élément manquant");
                }
            }
        }
    }
}
