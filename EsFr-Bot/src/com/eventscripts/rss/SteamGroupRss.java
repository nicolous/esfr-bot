/*
 * Copyright 2008-2011 Nicolas Maingot
 * 
 * This file is part of EsFr-Bot.
 *
 * EsFr-Bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * EsFr-Bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EsFr-Bot.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.eventscripts.rss;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.jibble.pircbot.Colors;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.eventscripts.pircbot.EsFrBot;

/** 
 * Lecture des flux RSS du groupe http://steamcommunity.com/groups/eventscripts-fr 
 */
public class SteamGroupRss extends RssChannelWatcher
{
    /** Le bot associé */
    private EsFrBot bot;
    
    /** Le channel qui lit le flux */
    private String channel;
    
    /**
     * @param ircBot Le bot IRC
     * @param ircChannel Le channel où afficher les messages
     * @see RssChannelWatcher
     * @throws RssChannelWatcherException
     */
    public SteamGroupRss(EsFrBot ircBot, String ircChannel, long watchInterval,
            SimpleDateFormat pubDateForm, String rssUrl) throws RssChannelWatcherException
    {
        super(watchInterval, pubDateForm, rssUrl);
        
        bot = ircBot;
        channel = ircChannel; 
    }

    @Override
    void read(ArrayList<Element> items)
    {
        int nbItems = items.size();
        if (nbItems > 0)
        {
            Element newerItem = items.get(0); // FIXME : RSS trié par date ?
            boolean valid = true;

            String title = "";
            NodeList titleNode = newerItem.getElementsByTagName("title");
            if (titleNode.getLength() > 0)
                title = titleNode.item(0).getTextContent();
            else
                valid = false;

            String link = "";
            NodeList linkNode = newerItem.getElementsByTagName("link");
            if (linkNode.getLength() > 0)
                link = linkNode.item(0).getTextContent();
            else
                valid = false;

            String description = "";
            NodeList descriptionNode = newerItem.getElementsByTagName("description");
            if (descriptionNode.getLength() > 0)
                description = descriptionNode.item(0).getTextContent();
            else
                valid = false;

            if (valid)
            {
                try
                {
                    String url = EsFrBot.shrinkUrl(link);
                    
                    description = description.replace("\n", " - ");
                    //bot.setTopic(channel, title + " (" + url + ")");
                    bot.sendRawLineViaQueue("PRIVMSG ChanServ :" + channel + " topic " + title + " ( " + url + " )");
                    bot.sendRawLineViaQueue("PRIVMSG ChanServ :" + channel + " set usergreeting News : " + title + " ( " + url + " )");
                    
                    String [] channels = bot.getChannels();
                    bot.globalMessage(channels, Colors.BOLD + "News : " + Colors.NORMAL + title + " ( " + url + " )"); 
                    //bot.globalMessage(channels, description + " ( " + url + " )");
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            else
                System.err.println(url + " : élément manquant");
        }
    }    
}
