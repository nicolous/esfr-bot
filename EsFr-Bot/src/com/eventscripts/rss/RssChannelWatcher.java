/*
 * Copyright 2008-2011 Nicolas Maingot
 * 
 * This file is part of EsFr-Bot.
 *
 * EsFr-Bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * EsFr-Bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EsFr-Bot.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.eventscripts.rss;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/** Lit un channel RSS à intervalles réguliers */
public abstract class RssChannelWatcher extends RssReader implements Runnable 
{
    /** Est-ce que la surveillance peut continuer ? */
    private boolean alive;
    
    /** Intervalle (en millisecondes) de temps entre deux lectures */
    private long interval;
    
    /** Date de la dernière lecture du channel */
    private long lastReadDate;
    
    /** Format du champ pubDate */
    private SimpleDateFormat pubDateFormat;

    /**
     * @param watchInterval Délai (en millisecondes) entre deux lectures du channel
     * @param pubDateForm Format du champ pubDate
     * @param rssUrl URL du channel
     */
    public RssChannelWatcher(long watchInterval, SimpleDateFormat pubDateForm, String rssUrl)
        throws RssChannelWatcherException
    {
        super(rssUrl);
        alive = true;
        interval = watchInterval;
        pubDateFormat = pubDateForm;
        
        lastReadDate = 0L;
        
        try
        {
            readNewItems();
        }
        catch(Exception e)
        {
            e.printStackTrace();
             throw new RssChannelWatcherException(e.getMessage());
        }
    }

    /** Récupère les items publiés depuis la dernière lecture
     * 
     * @param rssDocument Le document XML correspondant au flux RSS
     * @return La liste des nouveaux items
     * @throws RssChannelWatcherException
     * @throws ParseException
     */
    private ArrayList<Element> getNewItems(Document rssDocument) throws RssChannelWatcherException, ParseException
    {
        ArrayList<Element> items = new ArrayList<Element>();
        
        NodeList allItems = rssDocument.getElementsByTagName("item");
        int nbItems = allItems.getLength();
        
        long newerDate = lastReadDate;
        for (int i=0;i<nbItems;i++)
        {
            Element item = (Element)allItems.item(i);
            
            Node pubDateNode = item.getElementsByTagName("pubDate").item(0);
            if (pubDateNode != null)
            {
                String pubDate = pubDateNode.getTextContent();
                
                long releaseDate = pubDateFormat.parse(pubDate).getTime();
                //System.out.println("pubDate = " + releaseDate);
                //System.out.println("lastReadDate = " + lastReadDate);
                if (releaseDate > lastReadDate)
                {
                    if (releaseDate > newerDate)
                        newerDate = releaseDate;
                    
                    items.add(item);
                    //System.out.println("=> ok");
                }
                //System.out.println();
            }
            else
                throw new RssChannelWatcherException("pubDate introuvable");
        }
        
        lastReadDate = newerDate; 
        
        System.out.println(url + " : " + items.size() + " nouveaux items");
        
        return items;
    }
    
    /** Lit le flux RSS et récupère les items publiés depuis la dernière lecture
     * 
     * @return Une liste des nouveaux items
     * @throws Exception
     */
    public ArrayList<Element> readNewItems() throws Exception
    {
        // Ouverture du channel
        Document rssDocument = openChannel();

        // Récupération des nouveaux items
        return getNewItems(rssDocument);
    }
    
    @Override
    public void run()
    {
        while(alive)
        {
            try
            {
                // Attente de la prochaine lecture
                Thread.sleep(interval);
                
                try
                {
                    ArrayList<Element> items = readNewItems();
                    
                    // Appel de la callback
                    read(items);
                }
                catch(Exception e)
                {
                    System.err.println(url + " :");
                    e.printStackTrace();
                }                
            }
            catch(InterruptedException e)
            {
                System.err.println(url + " :");
                e.printStackTrace();
            }
        }
    }
    
    /** Automatiquement appelé lorsque la lecture est faite 
     * 
     * @param items Items trouvés sur le channel
     */
    abstract void read(ArrayList<Element> items);
    
    /** Stoppe définitivement la lecture du channel RSS */
    public void quit()
    {
        alive = false;
    }
}
