/*
 * Copyright 2008-2011 Nicolas Maingot
 * 
 * This file is part of EsFr-Bot.
 *
 * EsFr-Bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * EsFr-Bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EsFr-Bot.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.eventscripts.rss;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.jibble.pircbot.Colors;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.eventscripts.pircbot.EsFrBot;

/** 
 * Lecture du flux RSS d'EventScripts Addon Manager (http://addons.eventscripts.com)
 */
public class EsamRss extends RssChannelWatcher
{
    /** Le bot IRC */
    private EsFrBot bot;

    /**
     * @param ircBot Le bot IRC
     * @see RssChannelWatcher
     * @throws RssChannelWatcherException
     */
    public EsamRss(EsFrBot ircBot, long watchInterval, SimpleDateFormat pubDateForm,
            String rssUrl) throws RssChannelWatcherException
    {
        super(watchInterval, pubDateForm, rssUrl);
        bot = ircBot;
    }

    @Override
    void read(ArrayList<Element> items)
    {
        int nbItems = items.size();
        if (nbItems > 0)
        {
            for (int i = 0; i < nbItems; i++)
            {
                Element item = items.get(i);
                boolean valid = true;
    
                String title = "";
                NodeList titleNode = item.getElementsByTagName("title");
                if (titleNode.getLength() > 0)
                    title = titleNode.item(0).getTextContent();
                else
                    valid = false;

                String author = "";
                NodeList authorNode = item.getElementsByTagName("author");
                if (authorNode.getLength() > 0)
                    author = authorNode.item(0).getTextContent();
                else
                    valid = false;
                
                String link = "";
                NodeList linkNode = item.getElementsByTagName("link");
                if (linkNode.getLength() > 0)
                    link = linkNode.item(0).getTextContent();
                else
                    valid = false;
    
                String description = "";
                NodeList descriptionNode = item.getElementsByTagName("description");
                if (descriptionNode.getLength() > 0)
                    description = descriptionNode.item(0).getTextContent();
                else
                    valid = false;
    
                if (valid)
                {
                    try
                    {            
                        URL ri = new URL("http://addons.eventscripts.com/addons/chklatestver" 
                                    + link.substring(link.lastIndexOf("/"), link.length()));
                        
                        InputStream stream = ri.openStream();
                        BufferedReader streamReader = new BufferedReader(new InputStreamReader(stream));
                        String version = streamReader.readLine();
                        
                        String message = author + " vient de sortir la version " + version + " du script " +
                            Colors.BOLD + title + Colors.NORMAL + " ( " + EsFrBot.shrinkUrl(link) + " )";
                        if (description.length() > 0)
                            message += " : " + description;
                        bot.globalMessage(bot.getChannels(), message);
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                else
                    System.err.println(url + " : élément manquant");
            }
        }
    }
}
